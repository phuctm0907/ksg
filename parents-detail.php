<?php include 'header.php' ?>
    <section class="banner">
        <picture>
            <img src="./dist/image/Rectangle 197.png" alt="">
        </picture>
        <div class="text">
            <ul class="text-top">
                <li>
                    <a href="#">Home</a>
                    <span>></span>
                </li>
                <li>
                    <a href="#">Parents</a>
                    <span>></span>
                </li>
                <li>
                    <a href="#">Announcement</a>
                    <span>></span>
                </li>
            </ul>
            <div class="text-bot">
                <h2>Announcement</h2>
            </div>
        </div>
    </section>
    <section class="prarent-detail">
        <div class="container">
            <div class="row">

                <div class="col-md-8">
                    <div class="post-par">
                    <h3>KGS Photography Competition</h3>
                    <div class="date-of-share">
                        <div class="date">
                        <span><svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18"
                                   fill="none">
  <path d="M5.66667 3.79169C5.325 3.79169 5.04167 3.50835 5.04167 3.16669V0.666687C5.04167 0.32502 5.325 0.041687 5.66667 0.041687C6.00833 0.041687 6.29167 0.32502 6.29167 0.666687V3.16669C6.29167 3.50835 6.00833 3.79169 5.66667 3.79169Z"
        fill="#899197"/>
  <path d="M12.3333 3.79169C11.9917 3.79169 11.7083 3.50835 11.7083 3.16669V0.666687C11.7083 0.32502 11.9917 0.041687 12.3333 0.041687C12.675 0.041687 12.9583 0.32502 12.9583 0.666687V3.16669C12.9583 3.50835 12.675 3.79169 12.3333 3.79169Z"
        fill="#899197"/>
  <path d="M6.08333 11.0834C5.975 11.0834 5.86667 11.0584 5.76667 11.0167C5.65833 10.975 5.575 10.9167 5.49167 10.8417C5.34167 10.6834 5.25 10.475 5.25 10.25C5.25 10.1417 5.275 10.0334 5.31667 9.93336C5.35833 9.83336 5.41667 9.7417 5.49167 9.65837C5.575 9.58337 5.65833 9.52502 5.76667 9.48335C6.06667 9.35835 6.44167 9.42503 6.675 9.65837C6.825 9.8167 6.91667 10.0334 6.91667 10.25C6.91667 10.3 6.90833 10.3584 6.9 10.4167C6.89167 10.4667 6.875 10.5167 6.85 10.5667C6.83333 10.6167 6.80833 10.6667 6.775 10.7167C6.75 10.7584 6.70833 10.8 6.675 10.8417C6.51667 10.9917 6.3 11.0834 6.08333 11.0834Z"
        fill="#899197"/>
  <path d="M9 11.0834C8.89167 11.0834 8.78333 11.0584 8.68333 11.0167C8.575 10.975 8.49167 10.9167 8.40833 10.8417C8.25833 10.6833 8.16667 10.475 8.16667 10.25C8.16667 10.1417 8.19167 10.0333 8.23333 9.93335C8.275 9.83335 8.33333 9.74169 8.40833 9.65836C8.49167 9.58336 8.575 9.52501 8.68333 9.48334C8.98333 9.35001 9.35833 9.42502 9.59167 9.65836C9.74167 9.81669 9.83333 10.0334 9.83333 10.25C9.83333 10.3 9.825 10.3584 9.81667 10.4167C9.80833 10.4667 9.79166 10.5167 9.76666 10.5667C9.75 10.6167 9.725 10.6667 9.69167 10.7167C9.66667 10.7583 9.625 10.8 9.59167 10.8417C9.43333 10.9917 9.21667 11.0834 9 11.0834Z"
        fill="#899197"/>
  <path d="M11.9167 11.0834C11.8083 11.0834 11.7 11.0584 11.6 11.0167C11.4917 10.975 11.4083 10.9167 11.325 10.8417C11.2917 10.8 11.2583 10.7583 11.225 10.7167C11.1917 10.6667 11.1667 10.6167 11.15 10.5667C11.125 10.5167 11.1083 10.4667 11.1 10.4167C11.0917 10.3584 11.0833 10.3 11.0833 10.25C11.0833 10.0334 11.175 9.81669 11.325 9.65836C11.4083 9.58336 11.4917 9.52501 11.6 9.48334C11.9083 9.35001 12.275 9.42502 12.5083 9.65836C12.6583 9.81669 12.75 10.0334 12.75 10.25C12.75 10.3 12.7417 10.3584 12.7333 10.4167C12.725 10.4667 12.7083 10.5167 12.6833 10.5667C12.6667 10.6167 12.6417 10.6667 12.6083 10.7167C12.5833 10.7583 12.5417 10.8 12.5083 10.8417C12.35 10.9917 12.1333 11.0834 11.9167 11.0834Z"
        fill="#899197"/>
  <path d="M6.08333 14C5.975 14 5.86667 13.975 5.76667 13.9334C5.66667 13.8917 5.575 13.8334 5.49167 13.7584C5.34167 13.6 5.25 13.3834 5.25 13.1667C5.25 13.0584 5.275 12.95 5.31667 12.85C5.35833 12.7417 5.41667 12.65 5.49167 12.575C5.8 12.2667 6.36667 12.2667 6.675 12.575C6.825 12.7334 6.91667 12.95 6.91667 13.1667C6.91667 13.3834 6.825 13.6 6.675 13.7584C6.51667 13.9084 6.3 14 6.08333 14Z"
        fill="#899197"/>
  <path d="M9 14C8.78333 14 8.56667 13.9084 8.40833 13.7584C8.25833 13.6 8.16667 13.3834 8.16667 13.1667C8.16667 13.0584 8.19167 12.95 8.23333 12.85C8.275 12.7417 8.33333 12.65 8.40833 12.575C8.71667 12.2667 9.28333 12.2667 9.59167 12.575C9.66667 12.65 9.725 12.7417 9.76666 12.85C9.80833 12.95 9.83333 13.0584 9.83333 13.1667C9.83333 13.3834 9.74167 13.6 9.59167 13.7584C9.43333 13.9084 9.21667 14 9 14Z"
        fill="#899197"/>
  <path d="M11.9167 14C11.7 14 11.4833 13.9084 11.325 13.7584C11.25 13.6834 11.1917 13.5917 11.15 13.4834C11.1083 13.3834 11.0833 13.2751 11.0833 13.1667C11.0833 13.0584 11.1083 12.95 11.15 12.85C11.1917 12.7417 11.25 12.6501 11.325 12.5751C11.5167 12.3834 11.8083 12.2917 12.075 12.35C12.1333 12.3584 12.1833 12.375 12.2333 12.4C12.2833 12.4167 12.3333 12.4417 12.3833 12.4751C12.425 12.5001 12.4667 12.5417 12.5083 12.5751C12.6583 12.7334 12.75 12.9501 12.75 13.1667C12.75 13.3834 12.6583 13.6 12.5083 13.7584C12.35 13.9084 12.1333 14 11.9167 14Z"
        fill="#899197"/>
  <path d="M16.0833 7.19999H1.91667C1.575 7.19999 1.29167 6.91666 1.29167 6.57499C1.29167 6.23333 1.575 5.94999 1.91667 5.94999H16.0833C16.425 5.94999 16.7083 6.23333 16.7083 6.57499C16.7083 6.91666 16.425 7.19999 16.0833 7.19999Z"
        fill="#899197"/>
  <path d="M12.3333 17.9584H5.66667C2.625 17.9584 0.875 16.2084 0.875 13.1667V6.08335C0.875 3.04169 2.625 1.29169 5.66667 1.29169H12.3333C15.375 1.29169 17.125 3.04169 17.125 6.08335V13.1667C17.125 16.2084 15.375 17.9584 12.3333 17.9584ZM5.66667 2.54169C3.28333 2.54169 2.125 3.70002 2.125 6.08335V13.1667C2.125 15.55 3.28333 16.7084 5.66667 16.7084H12.3333C14.7167 16.7084 15.875 15.55 15.875 13.1667V6.08335C15.875 3.70002 14.7167 2.54169 12.3333 2.54169H5.66667Z"
        fill="#899197"/>
</svg></span>
                            <span>April 17, 2023</span>
                            <span>12:16</span>
                        </div>
                        <div class="share">
                            <span>Share</span>
                            <div class="icon">
                                <a href="">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                         fill="none">
                                        <path d="M24 11.9997C24 18.0603 19.5072 23.0706 13.6711 23.8839C13.1253 23.9599 12.567 23.9993 12.0003 23.9993C11.346 23.9993 10.7041 23.9468 10.078 23.8459C4.36357 22.9262 0 17.9725 0 11.9997C0 5.37241 5.37241 0 11.9997 0C18.6269 0 23.9993 5.37241 23.9993 11.9997H24Z"
                                              fill="#1877F7"/>
                                        <path d="M13.6731 9.63455V12.2485H16.9069L16.3949 15.7705H13.6731V23.8841C13.1272 23.9601 12.5689 23.9994 12.0023 23.9994C11.3479 23.9994 10.706 23.9469 10.0799 23.846V15.7705H7.09766V12.2485H10.0799V9.05066C10.0799 7.06615 11.6886 5.45685 13.6738 5.45685V5.45892C13.6793 5.45892 13.6848 5.45685 13.6903 5.45685H16.9076V8.50202H14.8056C14.1803 8.50202 13.6738 9.00851 13.6738 9.63386L13.6731 9.63455Z"
                                              fill="white"/>
                                    </svg>
                                </a>
                                <a href="">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                         fill="none">
                                        <path d="M23.9993 12.0003C23.9993 18.061 19.5065 23.0713 13.6705 23.8846C13.1246 23.9606 12.5663 24 11.9996 24C11.3453 24 10.7034 23.9475 10.0773 23.8466C4.36426 22.9269 0 17.9725 0 11.9997C0 5.37241 5.37241 0 11.9996 0C18.6269 0 23.9993 5.37241 23.9993 11.9997V12.0003Z"
                                              fill="#2188F7"/>
                                        <path d="M19.0922 7.60431C18.7591 7.94082 18.2975 8.14881 17.7862 8.14881C17.7213 8.14881 17.6563 8.14535 17.5934 8.13844C18.1269 7.81852 18.5387 7.31755 18.7439 6.71985C18.7439 6.71985 18.7446 6.71985 18.7446 6.71916C18.7729 6.639 18.7971 6.55816 18.8165 6.47455C18.3943 6.92576 17.8173 7.23118 17.1699 7.30373C17.0704 7.31548 16.9695 7.32101 16.8672 7.32101C16.8347 7.32101 16.8036 7.32031 16.7725 7.31824C16.5535 7.08814 16.2923 6.89812 16.0028 6.75854C15.6801 6.601 15.3194 6.50841 14.9387 6.49666C14.9076 6.49459 14.8765 6.4939 14.844 6.4939C14.5213 6.4939 14.2118 6.55125 13.9257 6.65697C13.8524 6.68392 13.7813 6.71294 13.7115 6.7468C12.8049 7.17175 12.1775 8.09284 12.1775 9.16041C12.1775 9.37945 12.2044 9.59228 12.2535 9.79543C11.9757 9.78437 11.7007 9.75742 11.4312 9.71734C11.3449 9.70352 11.2585 9.68901 11.1728 9.67243C10.4853 9.5446 9.82953 9.32486 9.22008 9.02636C8.67489 8.75964 8.16632 8.43073 7.70405 8.04792C7.31502 7.72731 6.95917 7.36937 6.64131 6.97828C6.62749 6.96169 6.61368 6.94442 6.60055 6.92784C6.58189 6.95617 6.56462 6.9845 6.54942 7.01421C6.32692 7.40323 6.20116 7.85306 6.20116 8.33261C6.20116 8.34712 6.20116 8.36301 6.20185 8.37752C6.20738 8.73684 6.28339 9.07887 6.41951 9.38982C6.47755 9.52801 6.54804 9.65861 6.62888 9.7823C6.86727 10.1492 7.19272 10.4546 7.57691 10.6661C7.59419 10.6771 7.61215 10.6861 7.63012 10.6944C7.60248 10.6999 7.57414 10.7048 7.54581 10.7075C7.46704 10.7179 7.3855 10.7234 7.30397 10.7234C6.8949 10.7234 6.51417 10.5956 6.20254 10.3779C6.20047 10.4007 6.20046 10.4242 6.20046 10.4477C6.20046 11.2092 6.5197 11.896 7.03103 12.3804C7.42904 12.7604 7.94382 13.0154 8.51596 13.09C8.31004 13.1654 8.08755 13.2061 7.85607 13.2061C7.60524 13.2061 7.36478 13.1585 7.14504 13.0707C7.47257 14.1355 8.44893 14.9163 9.61255 14.9509C8.90153 15.6895 7.90305 16.1483 6.79817 16.1483C6.11133 16.1483 5.46595 15.9708 4.90625 15.6598C6.2516 16.8241 8.00601 17.5276 9.92488 17.5276C14.1655 17.5276 17.6024 14.0906 17.6024 9.85001C17.6024 9.61646 17.5921 9.38567 17.5713 9.15765C18.2727 8.88056 18.8289 8.31464 19.0922 7.605V7.60431Z"
                                              fill="white"/>
                                    </svg>

                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="content-pos">
                        <img src="./dist/image/fcc1936d9713850882e18399468e6f3a.png" alt="">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
                        <br>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                            labore et dolore magna aliqua. Pellentesque habitant morbi tristique senectus. Arcu ac
                            tortor dignissim convallis. Maecenas sed enim ut sem viverra aliquet eget. Pellentesque
                            habitant morbi tristique senectus et netus et malesuada fames. Sed viverra tellus in hac
                            habitasse platea. Tempus quam pellentesque nec nam aliquam sem et tortor consequat. Id nibh
                            tortor id aliquet. Augue interdum velit euismod in pellentesque massa placerat duis. Mauris
                            ultrices eros in cursus turpis massa tincidunt dui.</p>
                        <br>
                        <br>
                        <h3>Sports for all</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                            labore et dolore magna aliqua. Pellentesque habitant morbi tristique senectus. Arcu ac
                            tortor dignissim convallis. Maecenas sed enim ut sem viverra aliquet eget. Pellentesque
                            habitant morbi tristique senectus et netus et malesuada fames. Sed viverra tellus in hac
                            habitasse platea. Tempus quam pellentesque nec nam aliquam sem et tortor consequat. Id nibh
                            tortor id aliquet. Augue interdum velit euismod in pellentesque massa placerat duis. Mauris
                            ultrices eros in cursus turpis massa tincidunt dui. Lorem ipsum dolor sit amet, consectetur
                            adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                            Pellentesque habitant morbi tristique senectus. Arcu ac tortor dignissim convallis. Maecenas
                            sed enim ut sem viverra aliquet eget. Pellentesque habitant morbi tristique senectus et
                            netus et malesuada fames. Sed viverra tellus in hac habitasse platea. Tempus quam
                            pellentesque nec nam aliquam sem et tortor consequat. Id nibh tortor id aliquet. Augue
                            interdum velit euismod in pellentesque massa placerat duis. Mauris ultrices eros in cursus
                            turpis massa tincidunt dui. </p>
                        <br>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                            labore et dolore magna aliqua. Pellentesque habitant morbi tristique senectus. Arcu ac
                            tortor dignissim convallis. Maecenas sed enim ut sem viverra aliquet eget. Pellentesque
                            habitant morbi tristique senectus et netus et malesuada fames. Sed viverra tellus in hac
                            habitasse platea. Tempus quam pellentesque nec nam aliquam sem et tortor consequat. Id nibh
                            tortor id aliquet. Augue interdum velit euismod in pellentesque massa placerat duis. Mauris
                            ultrices eros in cursus turpis massa tincidunt dui.</p>
                        <br>
                        <br>
                        <h3>Sports for all</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                            labore et dolore magna aliqua. Pellentesque habitant morbi tristique senectus. Arcu ac
                            tortor dignissim convallis. Maecenas sed enim ut sem viverra aliquet eget. Pellentesque
                            habitant morbi tristique senectus et netus et malesuada fames. Sed viverra tellus in hac
                            habitasse platea. Tempus quam pellentesque nec nam aliquam sem et tortor consequat. Id nibh
                            tortor id aliquet. Augue interdum velit euismod in pellentesque massa placerat duis. Mauris
                            ultrices eros in cursus turpis massa tincidunt dui. Lorem ipsum dolor sit amet, consectetur
                            adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                            Pellentesque habitant morbi tristique senectus. Arcu ac tortor dignissim convallis. Maecenas
                            sed enim ut sem viverra aliquet eget. Pellentesque habitant morbi tristique senectus et
                            netus et malesuada fames. Sed viverra tellus in hac habitasse platea. Tempus quam
                            pellentesque nec nam aliquam sem et tortor consequat. Id nibh tortor id aliquet. Augue
                            interdum velit euismod in pellentesque massa placerat duis. Mauris ultrices eros in cursus
                            turpis massa tincidunt dui. </p>
                        <br>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                            labore et dolore magna aliqua. Pellentesque habitant morbi tristique senectus. Arcu ac
                            tortor dignissim convallis. Maecenas sed enim ut sem viverra aliquet eget. Pellentesque
                            habitant morbi tristique senectus et netus et malesuada fames. Sed viverra tellus in hac
                            habitasse platea. Tempus quam pellentesque nec nam aliquam sem et tortor consequat. Id nibh
                            tortor id aliquet. Augue interdum velit euismod in pellentesque massa placerat duis. Mauris
                            ultrices eros in cursus turpis massa tincidunt dui.</p>

                    </div>
                    <div class="tag">
                        <span>#tag</span>
                        <span>#tag</span>
                        <span>#tag</span>
                    </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="related-articles">
                        <h4>Related articles</h4>
                        <div class="related-list">
                            <div class="item-related">
                                <a href="">
                                    <div class="img">
                                        <img src="./dist/image/fcc1936d9713850882e18399468e6f3a.png" alt="">
                                    </div>
                                    <div class="content">
                                        <div class="date">
                                            <span>2023 - 11 - 16</span>
                                        </div>
                                        <div class="title">
                                            <h4>Global Issues Research Contest</h4>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="item-related">
                                <a href="">
                                    <div class="img">
                                        <img src="./dist/image/fcc1936d9713850882e18399468e6f3a.png" alt="">
                                    </div>
                                    <div class="content">
                                        <div class="date">
                                            <span>2023 - 11 - 16</span>
                                        </div>
                                        <div class="title">
                                            <h4>Global Issues Research Contest</h4>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="item-related">
                                <a href="">
                                    <div class="img">
                                        <img src="./dist/image/fcc1936d9713850882e18399468e6f3a.png" alt="">
                                    </div>
                                    <div class="content">
                                        <div class="date">
                                            <span>2023 - 11 - 16</span>
                                        </div>
                                        <div class="title">
                                            <h4>Global Issues Research Contest</h4>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="item-related">
                                <a href="">
                                    <div class="img">
                                        <img src="./dist/image/fcc1936d9713850882e18399468e6f3a.png" alt="">
                                    </div>
                                    <div class="content">
                                        <div class="date">
                                            <span>2023 - 11 - 16</span>
                                        </div>
                                        <div class="title">
                                            <h4>Global Issues Research Contest</h4>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <script src="./dist/js/ad_regulation-process.js"></script>
<?php include 'footer.php' ?>