<?php include 'header.php' ?>
<section class="banner">
    <picture>
        <img src="./dist/image/Rectangle 197.png" alt="">
    </picture>
    <div class="text">
        <ul class="text-top">
            <li>
                <a href="#">Home</a>
                <span>></span>
            </li>
            <li>
                <a href="#">About Us</a>
                <span>></span>
            </li>
            <li>
                <a href="#">Mission & Vision</a>
                <span>></span>
            </li>
        </ul>
        <div class="text-bot">
            <h2>Principal’s Greeting</h2>
        </div>
    </div>
</section>
<section class="fa-question">
    <div class="container">
        <div class="fa-question-left">
            <div class="fa-question-left-item">
                <div class="title">
                    <h5>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod?</h5>
                    <div class="title-icon show-question-d">
                        <div class="close-question-d">
                            <picture>
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                    <path d="M18 12.75H6C5.59 12.75 5.25 12.41 5.25 12C5.25 11.59 5.59 11.25 6 11.25H18C18.41 11.25 18.75 11.59 18.75 12C18.75 12.41 18.41 12.75 18 12.75Z" fill="#0D487A"/>
                                </svg>
                            </picture>
                        </div>
                        <div class="open-question-d">
                            <picture >
                                <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14" fill="none">
                                    <path d="M13 7.75H1C0.59 7.75 0.25 7.41 0.25 7C0.25 6.59 0.59 6.25 1 6.25H13C13.41 6.25 13.75 6.59 13.75 7C13.75 7.41 13.41 7.75 13 7.75Z" fill="#0D487A"/>
                                    <path d="M7 13.75C6.59 13.75 6.25 13.41 6.25 13V1C6.25 0.59 6.59 0.25 7 0.25C7.41 0.25 7.75 0.59 7.75 1V13C7.75 13.41 7.41 13.75 7 13.75Z" fill="#0D487A"/>
                                </svg>
                            </picture>
                        </div>
                    </div>
                </div>
                <div class="content">
                    <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Pellentesque habitant morbi tristique senectus.</span>
                </div>
            </div>
            <div class="fa-question-left-item">
                <div class="title">
                    <h5>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod?</h5>
                    <div class="title-icon show-question-d">
                        <div class="close-question-d">
                            <picture>
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                    <path d="M18 12.75H6C5.59 12.75 5.25 12.41 5.25 12C5.25 11.59 5.59 11.25 6 11.25H18C18.41 11.25 18.75 11.59 18.75 12C18.75 12.41 18.41 12.75 18 12.75Z" fill="#0D487A"/>
                                </svg>
                            </picture>
                        </div>
                        <div class="open-question-d">
                            <picture >
                                <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14" fill="none">
                                    <path d="M13 7.75H1C0.59 7.75 0.25 7.41 0.25 7C0.25 6.59 0.59 6.25 1 6.25H13C13.41 6.25 13.75 6.59 13.75 7C13.75 7.41 13.41 7.75 13 7.75Z" fill="#0D487A"/>
                                    <path d="M7 13.75C6.59 13.75 6.25 13.41 6.25 13V1C6.25 0.59 6.59 0.25 7 0.25C7.41 0.25 7.75 0.59 7.75 1V13C7.75 13.41 7.41 13.75 7 13.75Z" fill="#0D487A"/>
                                </svg>
                            </picture>
                        </div>
                    </div>
                </div>
                <div class="content">
                    <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Pellentesque habitant morbi tristique senectus.</span>
                </div>
            </div>
            <div class="fa-question-left-item">
                <div class="title">
                    <h5>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod?</h5>
                    <div class="title-icon show-question-d">
                        <div class="close-question-d">
                            <picture>
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                    <path d="M18 12.75H6C5.59 12.75 5.25 12.41 5.25 12C5.25 11.59 5.59 11.25 6 11.25H18C18.41 11.25 18.75 11.59 18.75 12C18.75 12.41 18.41 12.75 18 12.75Z" fill="#0D487A"/>
                                </svg>
                            </picture>
                        </div>
                        <div class="open-question-d">
                            <picture >
                                <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14" fill="none">
                                    <path d="M13 7.75H1C0.59 7.75 0.25 7.41 0.25 7C0.25 6.59 0.59 6.25 1 6.25H13C13.41 6.25 13.75 6.59 13.75 7C13.75 7.41 13.41 7.75 13 7.75Z" fill="#0D487A"/>
                                    <path d="M7 13.75C6.59 13.75 6.25 13.41 6.25 13V1C6.25 0.59 6.59 0.25 7 0.25C7.41 0.25 7.75 0.59 7.75 1V13C7.75 13.41 7.41 13.75 7 13.75Z" fill="#0D487A"/>
                                </svg>
                            </picture>
                        </div>
                    </div>
                </div>
                <div class="content">
                    <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Pellentesque habitant morbi tristique senectus.</span>
                </div>
            </div>
        </div>
        <div class="fa-question-right">
            <div class="fa-question-right-title">
                <h4>Interested in seeing more? Let’s connect.</h4>
            </div>
            <div class="fa-question-right-form">
                <form action="">
                    <div>
                        <label class="required" for="name">Name</label>
                        <span>
                            <input type="text" id="name" placeholder="Nguyen Van A">    
                        </span>
                    </div>
                    <div>
                        <label class="required" for="email">Email</label>
                        <span>
                            <input type="text" id="email" placeholder="nguyenvana@gmail.com">    
                        </span>
                    </div>
                    <div>
                        <label class="required" for="phone_number">Phone number</label>
                        <span>
                            <input type="text" id="phone_number" placeholder="0123456789">    
                        </span>
                    </div>
                    <div>
                        <label class="required" for="question">Name</label>
                        <span>
                            <textarea name="" id="question" placeholder="Type here."></textarea>  
                        </span>
                    </div>
                    <input type="submit" value="Send" >
                </form>
            </div>
        </div>
    </div>
</section>
<?php include 'footer.php' ?>