<?php include 'header.php' ?>
    <section class="banner">
        <picture>
            <img src="./dist/image/Rectangle 197.png" alt="">
        </picture>
        <div class="text">
            <ul class="text-top">
                <li>
                    <a href="#">Home</a>
                    <span>></span>
                </li>
                <li>
                    <a href="#">Parents</a>
                    <span>></span>
                </li>
                <li>
                    <a href="#">Announcement</a>
                    <span>></span>
                </li>
            </ul>
            <div class="text-bot">
                <h2>Announcement</h2>
            </div>
        </div>
    </section>
    <section class="section-recruitment note-annoucement">
        <div class="container">
            <div class="input-search">
                <div class="col-md-8 col-12">
                    <div class="search">
                        <select class="form-select" aria-label="Default select example">
                            <option selected>Day</option>
                            <option value="1">One</option>
                            <option value="2">Two</option>
                            <option value="3">Three</option>
                        </select>
                        <select class="form-select" aria-label="Default select example">
                            <option selected>Month</option>
                            <option value="1">One</option>
                            <option value="2">Two</option>
                            <option value="3">Three</option>
                        </select>
                        <select class="form-select" aria-label="Default select example">
                            <option selected>Year</option>
                            <option value="1">One</option>
                            <option value="2">Two</option>
                            <option value="3">Three</option>
                        </select>
                        <input type="text" class="form-control" placeholder="Enter job keyword">
                        <button type="button" class="btn btn-secondary">Search</button>
                    </div>
                </div>

            </div>
            <div class="table">
                <table>
                    <tbody>
                    <tr>
                        <th>No</th>
                        <th>Title</th>
                        <th>Writer</th>
                        <th>Date</th>
                    </tr>
                    <tr>
                        <td>11</td>
                        <td>Global Issues Research Contest</td>
                        <td>Manager</td>
                        <td>2023.11.16</td>
                    </tr>
                    <tr>
                        <td>10</td>
                        <td>KGS Photography Competition</td>
                        <td>Manager</td>
                        <td>2023.11.16</td>
                    </tr>
                    <tr>
                        <td>9</td>
                        <td>KGS Sports DAY - 11/10</td>
                        <td>Manager</td>
                        <td>2023.11.16</td>
                    </tr>
                    <tr>
                        <td>8</td>
                        <td>Global Issues Research Contest</td>
                        <td>Manager</td>
                        <td>2023.11.16</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
    <script src="../dist/js/ad_regulation-process.js"></script>
<?php include 'footer.php' ?>