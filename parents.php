<?php include 'header.php' ?>
<section class="banner">
    <picture>
        <img src="./dist/image/Rectangle 197.png" alt="">
    </picture>
    <div class="text">
        <ul class="text-top">
            <li>
                <a href="#">Home</a>
                <span>></span>
            </li>
            <li>
                <a href="#">About Us</a>
                <span>></span>
            </li>
            <li>
                <a href="#">Mission & Vision</a>
                <span>></span>
            </li>
        </ul>
        <div class="text-bot">
            <h2>Principal’s Greeting</h2>
        </div>
    </div>
</section>
<section class="ad-regulation">
    <div class="container">
        <div class="row">
            <div class="col-3 ad-regulation-left">
                <div class="content">
                    <div class="title">
                        <h4>Admissions Regulations</h4>
                    </div>
                    <div class="align-items-start">
                        <ul class="list list-group">  <!--id="list-example" -->
                            <li><a class="active list-group-item list-group-item-action" href="#list-d1">Photo Gallery</a></li>    
                            <li><a class="list-group-item list-group-item-action" href="#list-d2">Annoucements</a></li>    
                            <li><a class="list-group-item list-group-item-action" href="#list-d3">Family Communication</a></li>    
                            <li><a class="list-group-item list-group-item-action" href="#list-d4">Press Release</a></li>    
                            <li><a class="list-group-item list-group-item-action" href="#list-d5">Daily Meal Photo</a></li>    
                        </ul>
                    </div>
                </div>
            </div> 
            <!-- data-bs-spy="scroll" data-bs-target="#list-example" data-bs-offset="0" tabindex="0" -->
            <div class="scrollspy-example ad-regulation-right col-8" >
                <div class="ad-regulation-right-item ad-process-right-item" id="list-d1">
                    <h4 class="title">Photo Gallery</h4>
                    <div class="text">
                        <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</span>
                    </div>
                    <div class="content-time">
                        <div class="time">
                            <div class="time-left">
                                <div class="time-left-date time-left-date-d1">
                                    <select id="day_d1">
                                        <option value="hide">Day</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                        <option value="10">10</option>
                                        <option value="11">11</option>
                                        <option value="12">12</option>
                                        <option value="13">13</option>
                                        <option value="14">14</option>
                                        <option value="15">15</option>
                                        <option value="16">16</option>
                                        <option value="17">17</option>
                                        <option value="18">18</option>
                                        <option value="19">19</option>
                                        <option value="20">20</option>
                                        <option value="21">21</option>
                                        <option value="22">22</option>
                                        <option value="23">23</option>
                                        <option value="24">24</option>
                                        <option value="25">25</option>
                                        <option value="26">26</option>
                                        <option value="27">27</option>
                                        <option value="28">28</option>
                                        <option value="29">29</option>
                                        <option value="30">30</option>
                                        <option value="31">31</option>
                                    </select> 
                                    <select id="month_d1">
                                        <option value="hide">Month</option>
                                        <option value="january">January</option>
                                        <option value="february">February</option>
                                        <option value="march">March</option>
                                        <option value="april">April</option>
                                        <option value="may">May</option>
                                        <option value="june">June</option>
                                        <option value="july">July</option>
                                        <option value="august">August</option>
                                        <option value="september">September</option>
                                        <option value="october">October</option>
                                        <option value="november">November</option>
                                        <option value="december">December</option>
                                    </select> 
                                    <select id="year_d1">
                                        <option value="hide">Year</option>
                                        <option value="2020">2020</option>
                                        <option value="2021">2021</option>
                                        <option value="2022">2022</option>
                                        <option value="2023">2023</option>
                                        <option value="2024">2024</option>
                                        <option value="2025">2025</option>
                                    </select>
                                </div>
                                <div class="time-left-button">
                                    <form action="#">
                                        <button type="submit" name="s">Search</button>
                                    </form>
                                    <a href="#">View more</a>
                                </div>
                            </div>
                            <div class="time-right">
                                <a href="#">View more</a>
                            </div>
                        </div>
                        <div class="slide">
                            <div class="swiper swiper-parents">
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide parents-list-item">
                                        <picture>
                                            <img src="./dist/image/Rectangle 254.png" alt="">
                                        </picture>
                                    </div>
                                    <div class="swiper-slide parents-list-item">
                                        <picture>
                                            <img src="./dist/image/Rectangle 254.png" alt="">
                                        </picture>
                                    </div>
                                    <div class="swiper-slide parents-list-item">
                                        <picture>
                                            <img src="./dist/image/Rectangle 254.png" alt="">
                                        </picture>
                                    </div>
                                    <div class="swiper-slide parents-list-item">
                                        <picture>
                                            <img src="./dist/image/Rectangle 254.png" alt="">
                                        </picture>
                                    </div>
                                    <div class="swiper-slide parents-list-item">
                                        <picture>
                                            <img src="./dist/image/Rectangle 254.png" alt="">
                                        </picture>
                                    </div>
                                    <div class="swiper-slide parents-list-item">
                                        <picture>
                                            <img src="./dist/image/Rectangle 254.png" alt="">
                                        </picture>
                                    </div>
                                </div>
                                <div class="swiper-button-next parents-button-next">
                                    <picture>
                                        <img src="./dist/image/arrow-right (1).png" alt="">
                                    </picture>
                                </div>
                                <div class="swiper-button-prev parents-button-prev">
                                    <picture>
                                        <img src="./dist/image/arrow-right.png" alt="">
                                    </picture>
                                </div>
                                <div class="swiper-pagination parents-pagination"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="ad-regulation-right-item ad-process-right-item" id="list-d2">
                    <h4 class="title">Photo Gallery</h4>
                    <div class="text">
                        <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</span>
                    </div>
                    <div class="content-time">
                        <div class="time">
                            <div class="time-left">
                                <div class="time-left-date">
                                    <select id="day_d2">
                                        <option value="hide">Day</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                        <option value="10">10</option>
                                        <option value="11">11</option>
                                        <option value="12">12</option>
                                        <option value="13">13</option>
                                        <option value="14">14</option>
                                        <option value="15">15</option>
                                        <option value="16">16</option>
                                        <option value="17">17</option>
                                        <option value="18">18</option>
                                        <option value="19">19</option>
                                        <option value="20">20</option>
                                        <option value="21">21</option>
                                        <option value="22">22</option>
                                        <option value="23">23</option>
                                        <option value="24">24</option>
                                        <option value="25">25</option>
                                        <option value="26">26</option>
                                        <option value="27">27</option>
                                        <option value="28">28</option>
                                        <option value="29">29</option>
                                        <option value="30">30</option>
                                        <option value="31">31</option>
                                    </select> 
                                    <select id="month_d2">
                                        <option value="hide">Month</option>
                                        <option value="january">January</option>
                                        <option value="february">February</option>
                                        <option value="march">March</option>
                                        <option value="april">April</option>
                                        <option value="may">May</option>
                                        <option value="june">June</option>
                                        <option value="july">July</option>
                                        <option value="august">August</option>
                                        <option value="september">September</option>
                                        <option value="october">October</option>
                                        <option value="november">November</option>
                                        <option value="december">December</option>
                                        <option value="december">December</option>
                                    </select> 
                                    <select id="year_d2">
                                        <option value="hide">Year</option>
                                        <option value="2020">2020</option>
                                        <option value="2021">2021</option>
                                        <option value="2022">2022</option>
                                        <option value="2023">2023</option>
                                        <option value="2024">2024</option>
                                        <option value="2025">2025</option>
                                    </select>
                                </div>
                                <div class="time-left-button">
                                    <form action="#">
                                        <button type="submit" name="s">Search</button>
                                    </form>
                                    <a href="#">View more</a>
                                </div>
                            </div>
                            <div class="time-right">
                                <a href="#">View more</a>
                            </div>
                        </div>
                        <div class="table">
                            <table>
                                <tbody>
                                    <tr>
                                        <th>No</th>
                                        <th>Title</th>
                                        <th>Writer</th>
                                        <th>Date</th>
                                    </tr>
                                    <tr>
                                        <td>11</td>
                                        <td>Global Issues Research Contest</td>
                                        <td>Manager</td>
                                        <td>2023.11.16</td>
                                    </tr>
                                    <tr>
                                        <td>10</td>
                                        <td>KGS Photography Competition</td>
                                        <td>Manager</td>
                                        <td>2023.11.16</td>
                                    </tr>
                                    <tr>
                                        <td>9</td>
                                        <td>KGS Sports DAY - 11/10</td>
                                        <td>Manager</td>
                                        <td>2023.11.16</td>
                                    </tr>
                                    <tr>
                                        <td>8</td>
                                        <td>Global Issues Research Contest</td>
                                        <td>Manager</td>
                                        <td>2023.11.16</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="ad-regulation-right-item ad-process-right-item" id="list-d3">
                    <h4 class="title">Photo Gallery</h4>
                    <div class="text">
                        <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</span>
                    </div>
                    <div class="content-time">
                        <div class="time">
                            <div class="time-left">
                                <div class="time-left-date">
                                    <select id="day_d3">
                                        <option value="hide">Day</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                        <option value="10">10</option>
                                        <option value="11">11</option>
                                        <option value="12">12</option>
                                        <option value="13">13</option>
                                        <option value="14">14</option>
                                        <option value="15">15</option>
                                        <option value="16">16</option>
                                        <option value="17">17</option>
                                        <option value="18">18</option>
                                        <option value="19">19</option>
                                        <option value="20">20</option>
                                        <option value="21">21</option>
                                        <option value="22">22</option>
                                        <option value="23">23</option>
                                        <option value="24">24</option>
                                        <option value="25">25</option>
                                        <option value="26">26</option>
                                        <option value="27">27</option>
                                        <option value="28">28</option>
                                        <option value="29">29</option>
                                        <option value="30">30</option>
                                        <option value="31">31</option>
                                    </select> 
                                    <select id="month_d3">
                                        <option value="hide">Month</option>
                                        <option value="january">January</option>
                                        <option value="february">February</option>
                                        <option value="march">March</option>
                                        <option value="april">April</option>
                                        <option value="may">May</option>
                                        <option value="june">June</option>
                                        <option value="july">July</option>
                                        <option value="august">August</option>
                                        <option value="september">September</option>
                                        <option value="october">October</option>
                                        <option value="november">November</option>
                                        <option value="december">December</option>
                                        <option value="december">December</option>
                                    </select> 
                                    <select id="year_d3">
                                        <option value="hide">Year</option>
                                        <option value="2020">2020</option>
                                        <option value="2021">2021</option>
                                        <option value="2022">2022</option>
                                        <option value="2023">2023</option>
                                        <option value="2024">2024</option>
                                        <option value="2025">2025</option>
                                    </select>
                                </div>
                                <div class="time-left-button">
                                    <form action="#">
                                        <button type="submit" name="s">Search</button>
                                    </form>
                                    <a href="#">View more</a>
                                </div>
                            </div>
                            <div class="time-right">
                                <a href="#">View more</a>
                            </div>
                        </div>
                        <div class="table">
                            <table>
                                <tbody>
                                    <tr>
                                        <th>No</th>
                                        <th>Title</th>
                                        <th>Writer</th>
                                        <th>Date</th>
                                    </tr>
                                    <tr>
                                        <td>11</td>
                                        <td>Global Issues Research Contest</td>
                                        <td>Manager</td>
                                        <td>2023.11.16</td>
                                    </tr>
                                    <tr>
                                        <td>10</td>
                                        <td>KGS Photography Competition</td>
                                        <td>Manager</td>
                                        <td>2023.11.16</td>
                                    </tr>
                                    <tr>
                                        <td>9</td>
                                        <td>KGS Sports DAY - 11/10</td>
                                        <td>Manager</td>
                                        <td>2023.11.16</td>
                                    </tr>
                                    <tr>
                                        <td>8</td>
                                        <td>Global Issues Research Contest</td>
                                        <td>Manager</td>
                                        <td>2023.11.16</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="ad-regulation-right-item ad-process-right-item" id="list-d4">
                    <h4 class="title">Photo Gallery</h4>
                    <div class="text">
                        <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</span>
                    </div>
                    <div class="content-time">
                        <div class="time">
                            <div class="time-left">
                                <div class="time-left-date">
                                    <select id="day_d4">
                                        <option value="hide">Day</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                        <option value="10">10</option>
                                        <option value="11">11</option>
                                        <option value="12">12</option>
                                        <option value="13">13</option>
                                        <option value="14">14</option>
                                        <option value="15">15</option>
                                        <option value="16">16</option>
                                        <option value="17">17</option>
                                        <option value="18">18</option>
                                        <option value="19">19</option>
                                        <option value="20">20</option>
                                        <option value="21">21</option>
                                        <option value="22">22</option>
                                        <option value="23">23</option>
                                        <option value="24">24</option>
                                        <option value="25">25</option>
                                        <option value="26">26</option>
                                        <option value="27">27</option>
                                        <option value="28">28</option>
                                        <option value="29">29</option>
                                        <option value="30">30</option>
                                        <option value="31">31</option>
                                    </select> 
                                    <select id="month_d4">
                                        <option value="hide">Month</option>
                                        <option value="january">January</option>
                                        <option value="february">February</option>
                                        <option value="march">March</option>
                                        <option value="april">April</option>
                                        <option value="may">May</option>
                                        <option value="june">June</option>
                                        <option value="july">July</option>
                                        <option value="august">August</option>
                                        <option value="september">September</option>
                                        <option value="october">October</option>
                                        <option value="november">November</option>
                                        <option value="december">December</option>
                                        <option value="december">December</option>
                                    </select> 
                                    <select id="year_d4">
                                        <option value="hide">Year</option>
                                        <option value="2020">2020</option>
                                        <option value="2021">2021</option>
                                        <option value="2022">2022</option>
                                        <option value="2023">2023</option>
                                        <option value="2024">2024</option>
                                        <option value="2025">2025</option>
                                    </select>
                                </div>
                                <div class="time-left-button">
                                    <form action="#">
                                        <button type="submit" name="s">Search</button>
                                    </form>
                                    <a href="#">View more</a>
                                </div>
                            </div>
                            <div class="time-right">
                                <a href="#">View more</a>
                            </div>
                        </div>
                        <div class="table">
                            <table>
                                <tbody>
                                    <tr>
                                        <th>No</th>
                                        <th>Title</th>
                                        <th>Writer</th>
                                        <th>Date</th>
                                    </tr>
                                    <tr>
                                        <td>11</td>
                                        <td>Global Issues Research Contest</td>
                                        <td>Manager</td>
                                        <td>2023.11.16</td>
                                    </tr>
                                    <tr>
                                        <td>10</td>
                                        <td>KGS Photography Competition</td>
                                        <td>Manager</td>
                                        <td>2023.11.16</td>
                                    </tr>
                                    <tr>
                                        <td>9</td>
                                        <td>KGS Sports DAY - 11/10</td>
                                        <td>Manager</td>
                                        <td>2023.11.16</td>
                                    </tr>
                                    <tr>
                                        <td>8</td>
                                        <td>Global Issues Research Contest</td>
                                        <td>Manager</td>
                                        <td>2023.11.16</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="ad-regulation-right-item ad-process-right-item" id="list-d5">
                    <h4 class="title">Photo Gallery</h4>
                    <div class="text">
                        <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</span>
                    </div>
                    <div class="content-time">
                        <div class="time">
                            <div class="time-left">
                                <div class="time-left-date">
                                    <select id="day_d5">
                                        <option value="hide">Day</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                        <option value="10">10</option>
                                        <option value="11">11</option>
                                        <option value="12">12</option>
                                        <option value="13">13</option>
                                        <option value="14">14</option>
                                        <option value="15">15</option>
                                        <option value="16">16</option>
                                        <option value="17">17</option>
                                        <option value="18">18</option>
                                        <option value="19">19</option>
                                        <option value="20">20</option>
                                        <option value="21">21</option>
                                        <option value="22">22</option>
                                        <option value="23">23</option>
                                        <option value="24">24</option>
                                        <option value="25">25</option>
                                        <option value="26">26</option>
                                        <option value="27">27</option>
                                        <option value="28">28</option>
                                        <option value="29">29</option>
                                        <option value="30">30</option>
                                        <option value="31">31</option>
                                    </select> 
                                    <select id="month_d5">
                                        <option value="hide">Month</option>
                                        <option value="january">January</option>
                                        <option value="february">February</option>
                                        <option value="march">March</option>
                                        <option value="april">April</option>
                                        <option value="may">May</option>
                                        <option value="june">June</option>
                                        <option value="july">July</option>
                                        <option value="august">August</option>
                                        <option value="september">September</option>
                                        <option value="october">October</option>
                                        <option value="november">November</option>
                                        <option value="december">December</option>
                                        <option value="december">December</option>
                                    </select> 
                                    <select id="year_d5">
                                        <option value="hide">Year</option>
                                        <option value="2020">2020</option>
                                        <option value="2021">2021</option>
                                        <option value="2022">2022</option>
                                        <option value="2023">2023</option>
                                        <option value="2024">2024</option>
                                        <option value="2025">2025</option>
                                    </select>
                                </div>
                                <div class="time-left-button">
                                    <form action="#">
                                        <button type="submit" name="s">Search</button>
                                    </form>
                                    <a href="#">View more</a>
                                </div>
                            </div>
                            <div class="time-right">
                                <a href="#">View more</a>
                            </div>
                        </div>
                        <div class="list">
                            <div class="list-gallery">
                                <picture>
                                    <img src="./dist/image/Rectangle 257.png" alt="">
                                </picture>
                                <picture>
                                    <img src="./dist/image/Rectangle 257.png" alt="">
                                </picture>
                                <picture>
                                    <img src="./dist/image/Rectangle 257.png" alt="">
                                </picture>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script src="./dist/js/ad_regulation-process.js"></script>
<?php include 'footer.php' ?>