<?php include 'header.php' ?>
<section class="banner">
    <picture>
        <img src="./dist/image/Rectangle 197.png" alt="">
    </picture>
    <div class="text">
        <ul class="text-top">
            <li>
                <a href="#">Home</a>
                <span>></span>
            </li>
            <li>
                <a href="#">About Us</a>
                <span>></span>
            </li>
            <li>
                <a href="#">Mission & Vision</a>
                <span>></span>
            </li>
        </ul>
        <div class="text-bot">
            <h2>Principal’s Greeting</h2>
        </div>
    </div>
</section>
<section class="about-history">
    <div class="container">
        <div class="about-history-list">
            <div class="about-history-list-item">
                <div class="content">
                    <div class="content_main aos-init aos-animate" data-aos="fade-left" data-aos-duration="1000" >
                        <div class="info">
                            <h3>2021. 02</h3>
                            <div class="text">
                                <span>Jae-ho Yang appointed as the first principal</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="about-history-list-item">
                <div class="content">
                    <div class="content_main aos-init aos-animate" data-aos="fade-right" data-aos-duration="1000">
                        <picture>
                            <img src="./dist/image/Rectangle 217.png" alt="">
                        </picture>
                        <div class="info">
                            <h3>2021. 02</h3>
                            <div class="text">
                                <span>Jae-ho Yang appointed as the first principal</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="about-history-list-item">
                <div class="content">
                    <div class="content_main aos-init aos-animate" data-aos="fade-left" data-aos-duration="1000" >
                        <picture>
                            <img src="./dist/image/Rectangle 217.png" alt="">
                        </picture>
                        <div class="info">
                            <h3>2021. 02</h3>
                            <div class="text">
                                <span>Jae-ho Yang appointed as the first principal</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="about-history-list-item">
                <div class="content">
                    <div class="content_main aos-init aos-animate" data-aos="fade-right" data-aos-duration="1000">
                        <picture>
                            <img src="./dist/image/Rectangle 217.png" alt="">
                        </picture>
                        <div class="info">
                            <h3>2021. 02</h3>
                            <div class="text">
                                <span>Jae-ho Yang appointed as the first principal</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="about-history-list-item">
                <div class="content">
                    <div class="content_main aos-init aos-animate" data-aos="fade-left" data-aos-duration="1000" >
                        <div class="info">
                            <h3>2021. 02</h3>
                            <div class="text">
                                <span>Jae-ho Yang appointed as the first principal</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="about-history-list-item">
                <div class="content">
                    <div class="content_main aos-init aos-animate" data-aos="fade-right" data-aos-duration="1000">
                        <picture>
                            <img src="./dist/image/Rectangle 217.png" alt="">
                        </picture>
                        <div class="info">
                            <h3>2021. 02</h3>
                            <div class="text">
                                <span>Jae-ho Yang appointed as the first principal</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="about-history-list-item">
                <div class="content">
                    <div class="content_main aos-init aos-animate" data-aos="fade-left" data-aos-duration="1000" >
                        <picture>
                            <img src="./dist/image/Rectangle 217.png" alt="">
                        </picture>
                        <div class="info">
                            <h3>2021. 02</h3>
                            <div class="text">
                                <span>Jae-ho Yang appointed as the first principal</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="about-history-list-item">
                <div class="content">
                    <div class="content_main aos-init aos-animate" data-aos="fade-right" data-aos-duration="1000">
                        <picture>
                            <img src="./dist/image/Rectangle 217.png" alt="">
                        </picture>
                        <div class="info">
                            <h3>2021. 02</h3>
                            <div class="text">
                                <span>Jae-ho Yang appointed as the first principal</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="about-history-list-item">
                <div class="content">
                    <div class="content_main aos-init aos-animate" data-aos="fade-left" data-aos-duration="1000" >
                        <picture>
                            <img src="./dist/image/Rectangle 217.png" alt="">
                        </picture>
                        <div class="info">
                            <h3>2021. 02</h3>
                            <div class="text">
                                <span>Jae-ho Yang appointed as the first principal</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="about-history-line"></div>
    </div>
</section>

<?php include 'footer.php' ?>