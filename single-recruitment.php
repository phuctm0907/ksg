<?php include 'header.php' ?>
    <section class="banner">
        <picture>
            <img src="./dist/image/Rectangle 197.png" alt="">
        </picture>
        <div class="text">
            <ul class="text-top">
                <li>
                    <a href="#">Home</a>
                    <span>></span>
                </li>
                <li>
                    <a href="#">Recruitment</a>
                </li>

            </ul>
            <div class="text-bot">
                <h2>Recruitment</h2>
            </div>
        </div>
    </section>
   <section class="single-recruitment">
       <div class="container">
           <div class="row info-recruitment">
               <div class="col-md-3">
                   <div class="table-info">
                       <div class="item">
                           <div class="info">
                               <li>Salary:</li>
                               <li><span>Negotiable</span></li>
                           </div>
                           <hr>
                       </div>
                       <div class="item">
                           <div class="info">
                               <li>Location::</li>
                               <li><span>Hanoi</span></li>
                           </div>
                           <hr>
                       </div>
                       <div class="item">
                           <div class="info">
                               <li>Application deadline::</li>
                               <li><span>21/11 — 27/12/2023</span></li>
                           </div>
                       </div>
                   </div>
                   <div class="info-more">
                       <h3>More positions</h3>
                       <div class="list-positions">
                           <div class="item-positions">
                               <div class="info">
                                   <a href=""><h4>Teacher of Maths</h4></a>
                                   <span>21/11 — 27/12/2023</span>
                               </div>
                               <hr>
                           </div>
                           <div class="item-positions">
                               <div class="info">
                                   <a href=""><h4>Teacher of Maths</h4></a>
                                   <span>21/11 — 27/12/2023</span>
                               </div>
                               <hr>
                           </div>
                       </div>
                       <div class="btn-link">
                           <a class="view-more" href="">View more jobs</a>
                       </div>
                   </div>
               </div>
               <div class="col-md-8">
                   <div class="title-post">
                       <h3>TEMPORARY MATHS CO-TEACHER</h3>
                       <p>KGS is looking to appoint a Teacher of DT (Senior School) to start in August 2024.</p>
                   </div>
                    <div class="content">
                        <h5>Job resposibilities:</h5>
                        <ul>
                            <li>Teaching PE in English (badminton, basketball, handball, football, swimming, nutrition ...) includes theory and practice for students in grades 6-12</li>
                            <li>Teaching PE in English (badminton, basketball, handball, football, swimming, nutrition ...) includes theory and practice for students in grades 6-12</li>
                            <li>Teaching PE in English (badminton, basketball, handball, football, swimming, nutrition ...) includes theory and practice for students in grades 6-12</li>
                            <li>Teaching PE in English (badminton, basketball, handball, football, swimming, nutrition ...) includes theory and practice for students in grades 6-12</li>
                            <li>Edit report for students (2 times/year)</li>
                            <li>Parent conferences for grades 6-12</li>
                            <li>Prepare exam questions and markers for grades 6-12</li>
                        </ul>
                    </div>
                   <div class="content">
                       <h5>Job resposibilities:</h5>
                       <ul>
                           <li>Teaching PE in English (badminton, basketball, handball, football, swimming, nutrition ...) includes theory and practice for students in grades 6-12</li>
                           <li>Teaching PE in English (badminton, basketball, handball, football, swimming, nutrition ...) includes theory and practice for students in grades 6-12</li>
                           <li>Teaching PE in English (badminton, basketball, handball, football, swimming, nutrition ...) includes theory and practice for students in grades 6-12</li>
                           <li>Teaching PE in English (badminton, basketball, handball, football, swimming, nutrition ...) includes theory and practice for students in grades 6-12</li>
                           <li>Edit report for students (2 times/year)</li>
                           <li>Parent conferences for grades 6-12</li>
                           <li>Prepare exam questions and markers for grades 6-12</li>
                       </ul>
                   </div>
                   <div class="content">
                       <h5>Job resposibilities:</h5>
                       <ul>
                           <li>Teaching PE in English (badminton, basketball, handball, football, swimming, nutrition ...) includes theory and practice for students in grades 6-12</li>
                           <li>Teaching PE in English (badminton, basketball, handball, football, swimming, nutrition ...) includes theory and practice for students in grades 6-12</li>
                           <li>Teaching PE in English (badminton, basketball, handball, football, swimming, nutrition ...) includes theory and practice for students in grades 6-12</li>
                           <li>Teaching PE in English (badminton, basketball, handball, football, swimming, nutrition ...) includes theory and practice for students in grades 6-12</li>
                           <li>Edit report for students (2 times/year)</li>
                           <li>Parent conferences for grades 6-12</li>
                           <li>Prepare exam questions and markers for grades 6-12</li>
                       </ul>
                   </div>
                   <hr>
                   <div class="form-apply">
                       <h3>Application form</h3>
                       <form action="">
                           <div class="label-input">
                               <label for="">Name <span>*</span></label>
                               <input type="text" placeholder="Nguyen Van A">
                           </div>
                           <div class="label-input">
                               <label for="">Email <span>*</span></label>
                               <input type="text" placeholder="nguyenvana@gmail.com">
                           </div>
                           <div class="label-input">
                               <label for="">Phone number <span>*</span></label>
                               <input type="text" placeholder="0123456789">
                           </div>
                           <div class="label-input">
                               <label for="">Content <span>*</span></label>
                               <input type="file">
                           </div>
                           <div class="btn-su">
                               <button type="submit">Submit</button>
                           </div>
                       </form>
                   </div>
               </div>
           </div>
       </div>
   </section>


<?php include 'footer.php' ?>