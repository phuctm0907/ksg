<footer class="footer">
    <picture>
        <img src="./dist/image/Untitled-1 1.png" alt="">
    </picture>
    <div class="container">
        <div class="footer-top">
            <div class="footer-top-left">
                <div class="logo">
                    <a href="#">
                        <picture>
                            <img src="./dist/image/Logo HN - dài - viền trắng - chữ trắng 1.png" alt="">
                        </picture>
                    </a>
                    <div class="text">
                        <p>We ara a lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididun.</p>
                    </div>
                </div>
                <div class="contact">
                    <div class="contact-top">
                        <div class="contact-top-item">
                            <picture>
                                <img src="./dist/image/call-calling.png" alt="">
                            </picture>
                            <div class="text">
                                <h5>Tel</h5>
                                <a href="#"><p>(+84) 24 3201 3131</p></a>
                            </div>
                        </div>
                        <div class="contact-top-item">
                            <picture>
                                <img src="./dist/image/call-calling.png" alt="">
                            </picture>
                            <div class="text">
                                <h5>Mail</h5>
                                <a href="#"><p>kgshanoi@kgs.edu.vn</p></a>
                            </div>
                        </div>
                        <div class="contact-top-item">
                            <picture>
                                <img src="./dist/image/call-calling.png" alt="">
                            </picture>
                            <div class="text">
                                <h5>Office</h5>
                                <a href="#"><p>(+84) 24 3201 3131</p></a>
                            </div>
                        </div>
                        <div class="contact-top-item">
                            <picture>
                                <img src="./dist/image/call-calling.png" alt="">
                            </picture>
                            <div class="text">
                                <h5>Address</h5>
                                <a href="#">
                                    <p>Số 1 Trịnh Văn Bô, Phương Canh, Quận Nam Từ Liêm, Hà Nội</p>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="contact-bot">
                        <a href="#" class="contact-bot-item">
                            <picture>
                                <img src="./dist/image/Group (5).png" alt="">
                            </picture>
                        </a>
                        <a href="#" class="contact-bot-item">
                            <picture>
                                <img src="./dist/image/Group (1).png" alt="">
                            </picture>
                        </a>
                        <a href="#" class="contact-bot-item">
                            <picture>
                                <img src="./dist/image/Group (2).png" alt="">
                            </picture>
                        </a>
                        <a href="#" class="contact-bot-item">
                            <picture>
                                <img src="./dist/image/Group (3).png" alt="">
                            </picture>
                        </a>
                        <a href="#" class="contact-bot-item">
                            <picture>
                                <img src="./dist/image/Group (4).png" alt="">
                            </picture>
                        </a>
                    </div>
                </div>
            </div>
            <div class="footer-top-right">
                <div class="footer-top-right-title">
                    <h4>Newsletter</h4>
                    <p>Be the first to receive new information about KGS and more other useful information</p>
                </div>
                <div class="footer-top-right-contact">
                    <div class="email">
                        <picture>
                            <img src="./dist/image/sms1.png" alt="">
                        </picture>
                        <span><input type="text" placeholder="Nhập email để nhận tin tức mới" ></span>
                        <input type="submit" value="Đăng ký">
                    </div>
                    <div class="map">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d4453.383236118185!2d105.78258832843579!3d20.979180788658105!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135accd88c1276b%3A0xc7ec85c744d8874e!2zSOG7kyBHxrDGoW0gUGxhemE!5e0!3m2!1svi!2s!4v1702519525613!5m2!1svi!2s" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-bot">
            <div class="footer-bot-left">
                <a href="#"><span>Annoucements</span></a>
                <a href="#"><span>Photo Gallery</span></a>
                <a href="#"><span>Daily Meal Photo</span></a>
                <a href="#"><span>Family Communication</span></a>
                <a href="#"><span>Press Release</span></a>
            </div>
            <div class="footer-bot-right">
                <a href="#"><span>© 2000-2023, All Rights Reserved</span></a>
            </div>
        </div>
    </div>
</footer>
<script src="./dist/bootstrap5/js/bootstrap.min.js"></script>
<script>
  AOS.init();
</script>
</body>
</html>
