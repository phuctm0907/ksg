<?php include 'header.php' ?>
<section class="banner">
    <picture>
        <img src="./dist/image/Rectangle 197.png" alt="">
    </picture>
    <div class="text">
        <ul class="text-top">
            <li>
                <a href="#">Home</a>
                <span>></span>
            </li>
            <li>
                <a href="#">About Us</a>
                <span>></span>
            </li>
            <li>
                <a href="#">Mission & Vision</a>
                <span>></span>
            </li>
        </ul>
        <div class="text-bot">
            <h2>Principal’s Greeting</h2>
        </div>
    </div>
</section>
<section class="ad-regulation">
    <div class="container">
        <div class="row">
            <div class="col-3 ad-regulation-left">
                <div class="content">
                    <div class="title">
                        <h4>Admissions Regulations</h4>
                    </div>
                    <div class="align-items-start">
                        <ul class="list list-group">  <!--id="list-example" -->
                            <li><a class="active list-group-item list-group-item-action" href="#list-d1">Admission priority order</a></li>    
                            <li><a class="list-group-item list-group-item-action" href="#list-d2">Age of enrolment</a></li>    
                            <li><a class="list-group-item list-group-item-action" href="#list-d3">Regulations on class capacity</a></li>    
                            <li><a class="list-group-item list-group-item-action" href="#list-d4">Admission registration conditions</a></li>    
                            <li><a class="list-group-item list-group-item-action" href="#list-d5">Admissions process</a></li>    
                            <li><a class="list-group-item list-group-item-action" href="#list-d6">Entrance exam</a></li>    
                            <li><a class="list-group-item list-group-item-action" href="#list-d7">Admission documents</a></li>    
                            <li><a class="list-group-item list-group-item-action" href="#list-d8">Enrolment registration and counselling</a></li>    
                        </ul>
                    </div>
                </div>
            </div> 
            <!-- data-bs-spy="scroll" data-bs-target="#list-example" data-bs-offset="0" tabindex="0" -->
            <div class="scrollspy-example ad-regulation-right col-8" >
                <div class="ad-regulation-right-item" id="list-d1">
                    <h4 class="title">Admission priority order</h4>
                    <div class="content">
                        <span>The order of admission priority is applied at Brighton College Vietnam (BCVN) campus with regards to available slots at the time of admission and prioritised in the following order:</span>
                        <div class="content-list">
                            <ul>
                                <li><span>1. Pupils who are children of officials, teachers and staff working at BCVN;</span></li>
                                <li><span>2. Pupils who are children of Vinhomes household residents;</span></li>
                                <li><span>3. Pupils who are children of officials, teachers and employees of Vingroup companies;</span></li>
                                <li><span>4. Pupils who are studying at Brighton College system;</span></li>
                                <li><span>5. Pupils who belong to other specified subjects.</span></li>
                            </ul>
                        </div>
                        
                    </div>
                </div>
                <div class="ad-regulation-right-item" id="list-d2">
                    <h4 class="title">Age of enrolment</h4>
                    <div class="content">
                        <span>In the school year 2023-2024, Brighton College Vietnam organises the enrolment of pupils of the age corresponding to each level of education as follows:</span>
                        <div class="content-list">
                            <ul>
                                <li><span>Pupils entering year 1: Born in 2018.</span></li>
                                <li><span>Pupils enrolling in years 2 to 10: according to their current grade level to transfer to ensure that</span></li>
                                <li><span>pupils continue their study path according to their birth year and capability.</span></li>
                            </ul>
                            <i>Note: For pupils who are studying in the 12-year system, pupils enter the school and the class corresponds to the age of the 13-year system (for example, pupils who complete grade 1 will enter Year 3).</i>
                        </div>
                    </div>
                </div>
                <div class="ad-regulation-right-item" id="list-d3">
                    <h4 class="title">Regulations on class capacity</h4>
                    <div class="content">
                        <div class="content-list">
                            <ul class="disc">
                                <li><span>Year 1 – Year 2: 20 pupils / class</span></li>
                                <li><span>Year 3 – Year 13: 24 pupils / class</span></li>
                            </ul>
                        </div>
                        <p>Under special circumstances, the number of pupils may be increased or decreased by 10%.</p>
                    </div>
                </div>
                <div class="ad-regulation-right-item" id="list-d4">
                    <h4 class="title">Admission registration conditions</h4>
                    <div class="content">
                        <div class="content-list">
                            <ul class="disc">
                                <li><span>Applicants must complete the previous year’s academic programme before admission or complete the first semester programme for those entering in the second semester.</span></li>
                                <li><span>The student should not have an infectious disease or a dangerous congenital disease. The school’s current support systems and facilities are unable to accommodate pupils with special needs.</span></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="ad-regulation-right-item" id="list-d5">
                    <h4 class="title">Admissions process</h4>
                    <div class="content">
                        <div class="content-list">
                            <ul>
                                <li><span>Step 1: Enrol your child at Parent.brightoncollege.edu.vn.</span></li>
                                <li><span>Step 2: The school admits pupils who are prioritised and meet the admission conditions.</span></li>
                                <li><span>Step 3: Pupils take the entrance exam.</span></li>
                                <li><span>Step 4: Parents complete the admission procedures for pupils who have passed the entrance exam.</span></li>
                                <li><span>Step 5: Pupils enter officially as notified by the school.</span></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="ad-regulation-right-item" id="list-d6">
                    <h4 class="title">Entrance exam</h4>
                    <div class="content">
                        <span>All new pupils must pass the entrance exam organised by the school.</span>
                        <div class="content-list">
                            <ul class="disc">
                                <li><span>For Year 1 pupils: Teachers engage in direct communication and interaction with pupils through chat activities, age-appropriate learning games, and discussions with pupils’ parents and/ guardians.</span></li>
                                <li><span>For Year 2 and above pupils: Pupils will take English language proficiency tests (except for Year 2 pupils), Logical Thinking and participate in face-to-face interviews with the school Board of Education. On the same day, interviews with pupils’ parents and/or guardians will also take place.</span></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="ad-regulation-right-item" id="list-d7">
                    <h4 class="title">Admission documents</h4>
                    <div class="content">
                        <table>
                            <tr>
                                <th>Item</th>
                                <th>For submission if admitted</th>
                            </tr>
                            <tr>
                                <td>Global Issues Research Contest</td>
                                <td>x</td>
                            </tr>
                            <tr>
                                <td>KGS Photography Competition</td>
                                <td>x</td>
                            </tr>
                            <tr>
                                <td>KGS Sports DAY - 11/10</td>
                                <td>x</td>
                            </tr>
                            <tr>
                                <td>Global Issues Research Contest</td>
                                <td>x</td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="ad-regulation-right-item" id="list-d8">
                    <h4 class="title">Enrolment registration and counselling</h4>
                    <div class="content">
                        <span>Parents who need advice on admission information please contact the admission hotline 18006010 or drop an email at info@brightoncollege.edu.vn.</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script src="./dist/js/ad_regulation-process.js"></script>
<?php include 'footer.php' ?>