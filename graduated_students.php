<?php include 'header.php' ?>
<section class="banner">
    <picture>
        <img src="./dist/image/Rectangle 197.png" alt="">
    </picture>
    <div class="text">
        <ul class="text-top">
            <li>
                <a href="#">Home</a>
                <span>></span>
            </li>
            <li>
                <a href="#">About Us</a>
                <span>></span>
            </li>
            <li>
                <a href="#">Mission & Vision</a>
                <span>></span>
            </li>
        </ul>
        <div class="text-bot">
            <h2>Principal’s Greeting</h2>
        </div>
    </div>
</section>
<section class="graduated-info">
    <div class="container">
        <picture>
            <img src="./dist/image/image 5.png" alt="">
        </picture>
        <div class="graduated-info-content">
            <h3>99% of students pass university admission</h3>
            <div class="text">
                Accumsan est in tempus etos ullamcorper sem quam suscipit lacus maecenas tortor. Suspendisse gravida ornare non mattis velit rutrum modest sed do eiusmod tempor incididunt ut labore et dolore est in tempus etos

                Accumsan est in tempus etos ullamcorper sem quam suscipit lacus maecenas tortor. Suspendisse gravida ornare non mattis velit rutrum modest sed do eiusmod tempor incididunt ut labore et dolore est in tempus etos

                Accumsan est in tempus etos ullamcorper sem quam suscipit lacus maecenas tortor. Suspendisse gravida ornare non mattis velit rutrum modest sed do eiusmod tempor incididunt ut labore et dolore est in tempus etos
            </div>
        </div>
    </div>
</section>
<section class="graduated-students">
    <div class="container">
        <div class="graduated-students-title">
            <h3>Our graduated students</h3>
        </div>
        <div class="graduated-students-list">
            <div class="swiper swiper-graduated-student">
                <div class="swiper-wrapper">
                    <div class="swiper-slide graduated-students-list-item">
                        <picture>
                            <img src="./dist/image/Rectangle 21100.png" alt="">
                        </picture>
                        <div class="content">
                            <h4>Jenny Wilson</h4>
                            <div class="text">
                                <span>Cambrigde University</span>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide graduated-students-list-item">
                        <picture>
                            <img src="./dist/image/Rectangle 21100.png" alt="">
                        </picture>
                        <div class="content">
                            <h4>Jenny Wilson</h4>
                            <div class="text">
                                <span>Cambrigde University</span>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide graduated-students-list-item">
                        <picture>
                            <img src="./dist/image/Rectangle 21100.png" alt="">
                        </picture>
                        <div class="content">
                            <h4>Jenny Wilson</h4>
                            <div class="text">
                                <span>Cambrigde University</span>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide graduated-students-list-item">
                        <picture>
                            <img src="./dist/image/Rectangle 21100.png" alt="">
                        </picture>
                        <div class="content">
                            <h4>Jenny Wilson</h4>
                            <div class="text">
                                <span>Cambrigde University</span>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide graduated-students-list-item">
                        <picture>
                            <img src="./dist/image/Rectangle 21100.png" alt="">
                        </picture>
                        <div class="content">
                            <h4>Jenny Wilson</h4>
                            <div class="text">
                                <span>Cambrigde University</span>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide graduated-students-list-item">
                        <picture>
                            <img src="./dist/image/Rectangle 21100.png" alt="">
                        </picture>
                        <div class="content">
                            <h4>Jenny Wilson</h4>
                            <div class="text">
                                <span>Cambrigde University</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-button-next students-button-next">
                    <picture>
                        <img src="./dist/image/arrow-right (1).png" alt="">
                    </picture>
                </div>
                <div class="swiper-button-prev students-button-prev">
                    <picture>
                        <img src="./dist/image/arrow-right.png" alt="">
                    </picture>
                </div>
                <div class="swiper-pagination students-pagination"></div>
            </div>
        </div>
    </div>
    
</section>
<?php include 'footer.php' ?>