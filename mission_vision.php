<?php include 'header.php' ?>
<section class="banner">
    <picture>
        <img src="./dist/image/Rectangle 197.png" alt="">
    </picture>
    <div class="text">
        <ul class="text-top">
            <li>
                <a href="#">Home</a>
                <span>></span>
            </li>
            <li>
                <a href="#">About Us</a>
                <span>></span>
            </li>
            <li>
                <a href="#">Mission & Vision</a>
                <span>></span>
            </li>
        </ul>
        <div class="text-bot">
            <h2>Principal’s Greeting</h2>
        </div>
    </div>
</section>
<section class="mission">
    <div class="container">
        <h3>Our Mission</h3>
        <ul class="mission-content">
            <li class="mission-content-item">
                <picture>
                    <img src="./dist/image/politics 1.png" alt="">
                </picture>
                <div class="text">
                    <h4>Diverse</h4>
                    <span>We are a diverse community of different nationalities, cultural and educational backgrounds, race, ethnicity, geographies, socio-economic background, gender roles, age, sexual orientation, religion, political views, neuro- divergences and physical differences, co-curricular abilities etc.</span>
                </div>
            </li>
            <li class="mission-content-item">
                <picture>
                    <img src="./dist/image/politics 1.png" alt="">
                </picture>
                <div class="text">
                    <h4>Diverse</h4>
                    <span>We are a diverse community of different nationalities, cultural and educational backgrounds, race, ethnicity, geographies, socio-economic background, gender roles, age, sexual orientation, religion, political views, neuro- divergences and physical differences, co-curricular abilities etc.</span>
                </div>
            </li>
            <li class="mission-content-item">
                <picture>
                    <img src="./dist/image/politics 1.png" alt="">
                </picture>
                <div class="text">
                    <h4>Diverse</h4>
                    <span>We are a diverse community of different nationalities, cultural and educational backgrounds, race, ethnicity, geographies, socio-economic background, gender roles, age, sexual orientation, religion, political views, neuro- divergences and physical differences, co-curricular abilities etc.</span>
                </div>
            </li>
        </ul>
    </div>
</section>
<section class="vision">
    <div class="container">
        <h3>Our Vision</h3>
        <div class="text">
            <p>To enable our students to thrivein a rapidly evolving world.</p>
        </div>
    </div>
    <picture>
        <img src="./dist/image/Group 1000006138.png" alt="">
    </picture>
</section>
<?php include 'footer.php' ?>