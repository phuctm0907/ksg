<?php include 'header.php' ?>
    <section class="banner">
        <picture>
            <img src="./dist/image/Rectangle 197.png" alt="">
        </picture>
        <div class="text">
            <ul class="text-top">
                <li>
                    <a href="#">Home</a>
                    <span>></span>
                </li>
                <li>
                    <a href="#">Parents</a>
                    <span>></span>
                </li>
                <li>
                    <a href="#">Photo Gallery</a>
                    <span>></span>
                </li>
            </ul>
            <div class="text-bot">
                <h2>Photo Gallery</h2>
            </div>
        </div>
    </section>
    <section class="ad-regulation gallery-section">
        <div class="container">
            <div class="row">
                <div class="col-3 ad-regulation-left">
                    <div class="content">
                        <div class="title">
                            <h4>Parents</h4>
                        </div>
                        <div class="align-items-start">
                            <ul class="list list-group">  <!--id="list-example" -->
                                <li><a class="active list-group-item list-group-item-action" href="#list-d1">Primary School</a></li>
                                <li><a class="list-group-item list-group-item-action" href="#list-d2">Secondary School</a></li>
                                <li><a class="list-group-item list-group-item-action" href="#list-d2">Sports</a></li>
                                <li><a class="list-group-item list-group-item-action" href="#list-d2">Events</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- data-bs-spy="scroll" data-bs-target="#list-example" data-bs-offset="0" tabindex="0" -->
                <div class="scrollspy-example ad-regulation-right col-8" >
                    <div class="ad-regulation-right-item" id="list-d1">

                        <div class="content-event-achieve">
                            <div class="event-achieve">
                                <a href="#" class="event-achieve-item galleryy-achieve-item">
                                    <picture>
                                        <img src="./dist/image/Rectangle 203.png" alt="">
                                    </picture>
                                    <div class="gallery-item">
                                        <div class="d-flex note-title">
                                            <div class="title-gallery">
                                                <h3>Extracurricular activities</h3>
                                                <span>210 Photos</span>
                                            </div>
                                            <div class="icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="32" height="33" viewBox="0 0 32 33" fill="none">
                                                    <path d="M11.8795 28.06C11.6261 28.06 11.3728 27.9667 11.1728 27.7667C10.7861 27.38 10.7861 26.74 11.1728 26.3534L19.8661 17.66C20.5061 17.02 20.5061 15.98 19.8661 15.34L11.1728 6.64669C10.7861 6.26003 10.7861 5.62003 11.1728 5.23336C11.5595 4.84669 12.1995 4.84669 12.5861 5.23336L21.2795 13.9267C21.9595 14.6067 22.3461 15.5267 22.3461 16.5C22.3461 17.4734 21.9728 18.3934 21.2795 19.0734L12.5861 27.7667C12.3861 27.9534 12.1328 28.06 11.8795 28.06Z" fill="#0D487A"/>
                                                </svg>
                                            </div>
                                        </div>

                                    </div>
                                </a>
                                <a href="#" class="event-achieve-item galleryy-achieve-item">
                                    <picture>
                                        <img src="./dist/image/Rectangle 203.png" alt="">
                                    </picture>
                                    <div class="gallery-item">
                                        <div class="d-flex note-title">
                                            <div class="title-gallery">
                                                <h3>Extracurricular activities</h3>
                                                <span>210 Photos</span>
                                            </div>
                                           <div class="icon">
                                               <svg xmlns="http://www.w3.org/2000/svg" width="32" height="33" viewBox="0 0 32 33" fill="none">
                                                   <path d="M11.8795 28.06C11.6261 28.06 11.3728 27.9667 11.1728 27.7667C10.7861 27.38 10.7861 26.74 11.1728 26.3534L19.8661 17.66C20.5061 17.02 20.5061 15.98 19.8661 15.34L11.1728 6.64669C10.7861 6.26003 10.7861 5.62003 11.1728 5.23336C11.5595 4.84669 12.1995 4.84669 12.5861 5.23336L21.2795 13.9267C21.9595 14.6067 22.3461 15.5267 22.3461 16.5C22.3461 17.4734 21.9728 18.3934 21.2795 19.0734L12.5861 27.7667C12.3861 27.9534 12.1328 28.06 11.8795 28.06Z" fill="#0D487A"/>
                                               </svg>
                                           </div>
                                        </div>

                                    </div>
                                </a>
                                <a href="#" class="event-achieve-item galleryy-achieve-item">
                                    <picture>
                                        <img src="./dist/image/Rectangle 203.png" alt="">
                                    </picture>
                                    <div class="gallery-item">
                                        <div class="d-flex note-title">
                                            <div class="title-gallery">
                                                <h3>Extracurricular activities</h3>
                                                <span>210 Photos</span>
                                            </div>
                                            <div class="icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="32" height="33" viewBox="0 0 32 33" fill="none">
                                                    <path d="M11.8795 28.06C11.6261 28.06 11.3728 27.9667 11.1728 27.7667C10.7861 27.38 10.7861 26.74 11.1728 26.3534L19.8661 17.66C20.5061 17.02 20.5061 15.98 19.8661 15.34L11.1728 6.64669C10.7861 6.26003 10.7861 5.62003 11.1728 5.23336C11.5595 4.84669 12.1995 4.84669 12.5861 5.23336L21.2795 13.9267C21.9595 14.6067 22.3461 15.5267 22.3461 16.5C22.3461 17.4734 21.9728 18.3934 21.2795 19.0734L12.5861 27.7667C12.3861 27.9534 12.1328 28.06 11.8795 28.06Z" fill="#0D487A"/>
                                                </svg>
                                            </div>
                                        </div>

                                    </div>
                                </a>
                                <a href="#" class="event-achieve-item galleryy-achieve-item">
                                    <picture>
                                        <img src="./dist/image/Rectangle 203.png" alt="">
                                    </picture>
                                    <div class="gallery-item">
                                        <div class="d-flex note-title">
                                            <div class="title-gallery">
                                                <h3>Extracurricular activities</h3>
                                                <span>210 Photos</span>
                                            </div>
                                            <div class="icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="32" height="33" viewBox="0 0 32 33" fill="none">
                                                    <path d="M11.8795 28.06C11.6261 28.06 11.3728 27.9667 11.1728 27.7667C10.7861 27.38 10.7861 26.74 11.1728 26.3534L19.8661 17.66C20.5061 17.02 20.5061 15.98 19.8661 15.34L11.1728 6.64669C10.7861 6.26003 10.7861 5.62003 11.1728 5.23336C11.5595 4.84669 12.1995 4.84669 12.5861 5.23336L21.2795 13.9267C21.9595 14.6067 22.3461 15.5267 22.3461 16.5C22.3461 17.4734 21.9728 18.3934 21.2795 19.0734L12.5861 27.7667C12.3861 27.9534 12.1328 28.06 11.8795 28.06Z" fill="#0D487A"/>
                                                </svg>
                                            </div>
                                        </div>

                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script src="./dist/js/ad_regulation-process.js"></script>
<?php include 'footer.php' ?>