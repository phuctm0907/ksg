<?php include 'header.php' ?>
<section class="banner">
    <picture>
        <img src="./dist/image/Rectangle 197.png" alt="">
    </picture>
    <div class="text">
        <ul class="text-top">
            <li>
                <a href="#">Home</a>
                <span>></span>
            </li>
            <li>
                <a href="#">About Us</a>
                <span>></span>
            </li>
            <li>
                <a href="#">Mission & Vision</a>
                <span>></span>
            </li>
        </ul>
        <div class="text-bot">
            <h2>Principal’s Greeting</h2>
        </div>
    </div>
</section>
<section class="core-value-our aos-init aos-animate" data-aos="zoom-in" data-aos-duration="1000">
    <div class="container">
        <div class="core-value-our-title">
            <h3>Our core values</h3>
            <div class="text">
                <span>Portrait of a KGS student: Thoughtful, adept decision-maker, confidently conducts themselves, outstanding capabilities, high adaptability, respects oneself and others.
                    Our values underpin our vision of enriching the lives of children to achieve more than they believe they can. They are our building blocks for our culture of CARE and enable our diverse, internationally-minded community to flourish as energized, engaged and empowered learners.
                </span>
            </div>
        </div>
        <picture>
            <img src="./dist/image/Rectangle 2618 (1).png" alt="">
        </picture>
    </div>
</section>
<section class="core-value-info aos-init aos-animate" data-aos="zoom-in" data-aos-duration="1000">
    <div class="container">
        <ul class="core-value-info-list">
            <li class="core-value-info-list-item">
                <div class="icon"><p>D</p></div>
                <div class="title">
                    <h5>Diversity</h5>
                    <div class="text">
                        <p></p>
                        <span>We flourish and find fulfillment in meaningful and worthwhile tasks, and connecting with others at a deeper level—in essence, living the “good life”</span>
                    </div>
                </div>
            </li>
            <li class="core-value-info-list-item">
                <div class="icon"><p>R</p></div>
                <div class="title">
                    <h5>Diversity</h5>
                    <div class="text">
                        <p></p>
                        <span>We flourish and find fulfillment in meaningful and worthwhile tasks, and connecting with others at a deeper level—in essence, living the “good life”</span>
                    </div>
                </div>
            </li>
            <li class="core-value-info-list-item">
                <div class="icon"><p>E</p></div>
                <div class="title">
                    <h5>Diversity</h5>
                    <div class="text">
                        <p></p>
                        <span>We flourish and find fulfillment in meaningful and worthwhile tasks, and connecting with others at a deeper level—in essence, living the “good life”</span>
                    </div>
                </div>
            </li>
            <li class="core-value-info-list-item">
                <div class="icon"><p>A</p></div>
                <div class="title">
                    <h5>Diversity</h5>
                    <div class="text">
                        <p></p>
                        <span>We flourish and find fulfillment in meaningful and worthwhile tasks, and connecting with others at a deeper level—in essence, living the “good life”</span>
                    </div>
                </div>
            </li>
            <li class="core-value-info-list-item">
                <div class="icon"><p>M</p></div>
                <div class="title">
                    <h5>Diversity</h5>
                    <div class="text">
                        <p></p>
                        <span>We flourish and find fulfillment in meaningful and worthwhile tasks, and connecting with others at a deeper level—in essence, living the “good life”</span>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</section>
<?php include 'footer.php' ?>