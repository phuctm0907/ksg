<?php include 'header.php' ?>
    <section class="banner">
        <picture>
            <img src="./dist/image/Rectangle 197.png" alt="">
        </picture>
        <div class="text">
            <ul class="text-top">
                <li>
                    <a href="#">Home</a>
                    <span>></span>
                </li>
                <li>
                    <a href="#">Contact Us</a>
                </li>

            </ul>
            <div class="text-bot">
                <h2>Contact Us</h2>
            </div>
        </div>
    </section>
    <section class="section-contact">
        <div class="container">
            <div class="row live-contact">
                <div class="col-5">
                    <div class="contact-right">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.6985148455115!2d105.81129166107256!3d21.00471893852265!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ac9b8403c2b9%3A0x62dd79b94e971d08!2zU8OibiBiw7NuZyBHacOhcCBOaOG6pXQsIDI5IE5nw6FjaCAyMTMsIFThu5UgMSwgVGhhbmggWHXDom4sIEjDoCBO4buZaSwgVmnhu4d0IE5hbQ!5e0!3m2!1svi!2s!4v1703063152188!5m2!1svi!2s" width="100%" height="277" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>

                        <div class="contact">
                            <div class="contact-top-item">
                                <picture>
                                    <img src="./dist/image/call-calling.png" alt="">
                                </picture>
                                <div class="text">
                                    <h5>Tel</h5>
                                    <a href="#"><p>(+84) 24 3201 3131</p></a>
                                </div>
                            </div>
                            <div class="contact-top-item">
                                <picture>
                                    <img src="./dist/image/call-calling.png" alt="">
                                </picture>
                                <div class="text">
                                    <h5>Mail</h5>
                                    <a href="#"><p>kgshanoi@kgs.edu.vn</p></a>
                                </div>
                            </div>
                            <div class="contact-top-item">
                                <picture>
                                    <img src="./dist/image/call-calling.png" alt="">
                                </picture>
                                <div class="text">
                                    <h5>Office</h5>
                                    <a href="#"><p>(+84) 24 3201 3131</p></a>
                                </div>
                            </div>
                            <div class="contact-top-item">
                                <picture>
                                    <img src="./dist/image/call-calling.png" alt="">
                                </picture>
                                <div class="text">
                                    <h5>Address</h5>
                                    <a href="#">
                                        <p>Số 1 Trịnh Văn Bô, Phương Canh, Quận Nam Từ Liêm, Hà Nội</p>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-6">
                    <div class="contact-left">
                        <div class="contact-form">
                            <form action="">
                                <div class="label-input">
                                    <label for="">Name <span>*</span></label>
                                    <input type="text" placeholder="Nguyen Van A">
                                </div>
                                <div class="label-input">
                                    <label for="">Email <span>*</span></label>
                                    <input type="text" placeholder="nguyenvana@gmail.com">
                                </div>
                                <div class="label-input">
                                    <label for="">Phone number <span>*</span></label>
                                    <input type="text" placeholder="0123456789">
                                </div>
                                <div class="label-input">
                                    <label for="">Content <span>*</span></label>
                                    <textarea name="content" placeholder="Type here." id="" cols="30" rows="10"></textarea>
                                </div>
                                <div class="btn-su">
                                    <button type="submit">Send</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php include 'footer.php' ?>