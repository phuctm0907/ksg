<?php include 'header.php'; ?>
<main class="main" xmlns="http://www.w3.org/1999/html">
    <section class="banner-home">
        <div class="slider-banner">
            <div class="swiper mySwiper">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <video id="image-detail-layer" autoplay="autoplay" playsinline=""
                               class="image--detail bg-video bg-video--fullscreen" loop="loop" muted="false"
                               preload="auto" src="./dist/video/videobanner.mp4"></video>
                    </div>
                    <div class="swiper-slide">
                        <img src="./dist/video/Rectangle197.png" alt="">
                    </div>
                    <div class="swiper-slide"><img src="./dist/video/Rectangle197.png" alt=""></div>
                    <div class="swiper-slide">
                        <video id="image-detail-layer" autoplay="autoplay" playsinline=""
                               class="image--detail bg-video bg-video--fullscreen" loop="loop" muted="false"
                               preload="auto" src="./dist/video/videobanner.mp4"></video>
                    </div>
                </div>
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
                <div class="swiper-pagination"></div>
            </div>
        </div>
    </section>
    <section class="section-about">
        <div class="container">
            <div class="content">
                <div class="row">
                    <div class="col-md-6">
                        <div class="img-about">
                            <img class="img-1" src="./dist/image/607420e040a6a286979b8f08fba151fc.png" alt="">
                            <img class="img-2" src="./dist/image/a96288cab0e875583663ca270ad82843.png" alt="">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="content-des">
                            <div class="title">
                                <h3>A school of DREAM</h3>
                                <p>We are proud to be a part of the KGS family. In their Independent Schools Guide,
                                    widely acknowledged as the most authoritative survey of the top schools in the
                                    Korean, The Sunday Times names the College as “one of the hottest tickets in
                                    independent education.”</p>
                            </div>
                            <div class="note">
                                <h5>Conscientious, tell the truth, do the truth and test truthfully</h5>
                                <p>We are proud to be a part of the KGS family. In their Independent Schools Guide,
                                    widely acknowledged as the most authoritative survey of the top schools in the
                                    Korean, The Sunday Times names the College</p>
                            </div>
                            <div class="note">
                                <h5>Nurturing global talent by personality</h5>
                                <p>We are proud to be a part of the KGS family. In their Independent Schools Guide,
                                    widely acknowledged as the most authoritative survey of the top schools in the
                                    Korean, The Sunday Times names the College</p>
                            </div>
                            <div class="btn-more">
                                <a href="">View more detail
                                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="14" viewBox="0 0 20 14"
                                         fill="none">
                                        <path d="M12.43 13.82C12.24 13.82 12.05 13.75 11.9 13.6C11.61 13.31 11.61 12.83 11.9 12.54L17.44 7L11.9 1.46C11.61 1.17 11.61 0.689995 11.9 0.399995C12.19 0.109995 12.67 0.109995 12.96 0.399995L19.03 6.47C19.32 6.76 19.32 7.24 19.03 7.52999L12.96 13.6C12.81 13.75 12.62 13.82 12.43 13.82Z"
                                              fill="white"/>
                                        <path d="M18.33 7.75H1.5C1.09 7.75 0.75 7.41 0.75 7C0.75 6.59 1.09 6.25 1.5 6.25H18.33C18.74 6.25 19.08 6.59 19.08 7C19.08 7.41 18.74 7.75 18.33 7.75Z"
                                              fill="white"/>
                                    </svg>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section-parent">
        <div class="container">
            <div class="title">
                <h3>Parents</h3>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem  </br> Ipsum has
                    been the
                    industry's standard dummy</p>
            </div>
            <div class="content">
                <div class="row">
                    <div class="col-xl-4">
                        <div class="swiper sliderParents">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <div class="img">
                                        <figure>
                                            <img alt="Kết cấu thép"
                                                 src="http://hailongjsc.wecan-group.info/wp-content/uploads/2023/09/banner-ket-cau-thep.png">
                                        </figure>
                                    </div>
                                    <div class="description">
                                        <span>Photo Gallery</span>
                                        <h3>Notice of change in class time at KGS Hanoi from December 20</h3>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="img">
                                        <figure>
                                            <img alt="Kết cấu thép"
                                                 src="http://hailongjsc.wecan-group.info/wp-content/uploads/2023/09/banner-ket-cau-thep.png">
                                        </figure>
                                    </div>
                                    <div class="description">
                                        <span>Photo Gallery</span>
                                        <h3>Notice of change in class time at KGS Hanoi from December 20</h3>

                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="img">
                                        <figure>
                                            <img alt="Kết cấu thép"
                                                 src="http://hailongjsc.wecan-group.info/wp-content/uploads/2023/09/banner-ket-cau-thep.png">
                                        </figure>
                                    </div>
                                    <div class="description">
                                        <span>Photo Gallery</span>
                                        <h3>Notice of change in class time at KGS Hanoi from December 20</h3>

                                    </div>
                                </div>

                            </div>
                            <div class="swiper-pagination"></div>
                        </div>
                    </div>
                    <div class="col-xl-4">
                        <div class="item-col">
                            <div class="title-qq">
                                <h4>Announcement</h4>
                                <hr>
                            </div>
                            <div class="content-qq">
                                <ul>
                                    <li><p>Global Issues Research Contest</p><span>2023-11-07</span></li>
                                    <li><p>KGS Photography Competition</p><span>2023-11-07</span></li>
                                    <li><p>Global Issues Research Contest</p><span>2023-11-07</span></li>
                                    <li><p>KGS Sports DAY - 11/10</p><span>2023-11-07</span></li>
                                    <li><p>Korean Math Olympiad...</p><span>2023-11-07</span></li>
                                </ul>
                            </div>
                        </div>
                        <div class="item-col">
                            <div class="title-qq">
                                <h4>Announcement</h4>
                                <hr>
                            </div>
                            <div class="content-qq">
                                <div class="div-img">
                                    <div class="img">
                                        <img src="./dist/image/8d6c14a771f2cbb8325d6fd4ad467348.png" alt="">
                                    </div>
                                    <div class="img">
                                        <img src="./dist/image/8d28a1772a85958d5ba1e6cd211cb374.png" alt="">
                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-xl-4">
                        <div class="item-col">
                            <div class="title-qq">
                                <h4>Announcement</h4>
                                <hr>
                            </div>
                            <div class="content-qq">
                                <ul>
                                    <li><p>Global Issues Research Contest</p><span>2023-11-07</span></li>
                                    <li><p>KGS Photography Competition</p><span>2023-11-07</span></li>
                                    <li><p>Global Issues Research Contest</p><span>2023-11-07</span></li>
                                    <li><p>KGS Sports DAY - 11/10</p><span>2023-11-07</span></li>
                                    <li><p>Korean Math Olympiad...</p><span>2023-11-07</span></li>
                                </ul>
                            </div>
                        </div>
                        <div class="item-col">
                            <div class="title-qq">
                                <h4>Announcement</h4>
                                <hr>
                            </div>
                            <div class="content-qq">
                                <ul>
                                    <li><p>Global Issues Research Contest</p><span>2023-11-07</span></li>
                                    <li><p>KGS Photography Competition</p><span>2023-11-07</span></li>
                                    <li><p>Global Issues Research Contest</p><span>2023-11-07</span></li>
                                    <li><p>KGS Sports DAY - 11/10</p><span>2023-11-07</span></li>
                                    <li><p>Korean Math Olympiad...</p><span>2023-11-07</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="btn-more">
                    <a href="">View more detail
                        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="14" viewBox="0 0 20 14"
                             fill="none">
                            <path d="M12.43 13.82C12.24 13.82 12.05 13.75 11.9 13.6C11.61 13.31 11.61 12.83 11.9 12.54L17.44 7L11.9 1.46C11.61 1.17 11.61 0.689995 11.9 0.399995C12.19 0.109995 12.67 0.109995 12.96 0.399995L19.03 6.47C19.32 6.76 19.32 7.24 19.03 7.52999L12.96 13.6C12.81 13.75 12.62 13.82 12.43 13.82Z"
                                  fill="white"/>
                            <path d="M18.33 7.75H1.5C1.09 7.75 0.75 7.41 0.75 7C0.75 6.59 1.09 6.25 1.5 6.25H18.33C18.74 6.25 19.08 6.59 19.08 7C19.08 7.41 18.74 7.75 18.33 7.75Z"
                                  fill="white"/>
                        </svg>
                    </a>
                </div>
            </div>
        </div>
        </div>
    </section>
    <section class="section-why">
        <div class="container">
            <div class="why-choose">
                <div class="d-flex">
                    <div class="item">
                        <div class="content">
                            <div class="note">
                                <h3>Why choose us</h3>
                                <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a
                                    piece of classical Latin literature from 45 BC, making it over 2000 years old.
                                    Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked
                                    up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and
                                    going through the cites of the word in classical literature, discovered the
                                    undoubtable source. </p>
                            </div>

                        </div>
                    </div>
                    <div class="item">
                        <div class="img">
                            <img src="./dist/image/2df0fd628c87e470c02fabb6376abd04.png" alt="">
                        </div>
                    </div>
                </div>
                <div class="data">
                    <div class="item-data">
                        <div class="img">
                            <img src="./dist/image/Asset%202%201.svg" alt="">
                        </div>
                        <div class="solieu">
                            <h2>163</h2>
                            <p>Teachers</p>
                        </div>
                    </div>
                    <div class="item-data">
                        <div class="img">
                            <img src="./dist/image/Asset%202%201.svg" alt="">
                        </div>
                        <div class="solieu">
                            <h2>163</h2>
                            <p>Teachers</p>
                        </div>
                    </div>
                    <div class="item-data">
                        <div class="img">
                            <img src="./dist/image/Asset%202%201.svg" alt="">
                        </div>
                        <div class="solieu">
                            <h2>163</h2>
                            <p>Teachers</p>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </section>
    <section class="section-what">
        <div class="container">
            <div class="content-what">
                <div class="row">
                    <div class="col-xl-7 note-ng">
                        <div class="img">
                            <img src="./dist/image/6a311d38976c699d10a64faf967121b0.png" alt="">
                        </div>
                    </div>
                    <div class="col-xl-5 note-ng">
                        <div class="content">
                            <div class="title">
                                <h3>What do Ss love </br>
                                    about KGS</h3>
                                <p>There are many variations of passages of Lorem Ipsum available, but the majority have
                                    suffered alteration in some form, by injected humour, or randomised words</p>
                            </div>
                            <div class="sliderlive swiper">
                                <div class="swiper-wrapper">
                                    <div class="swiper-slide">
                                        <p>We are United CFO, We provide Finance consulting from 30 years. Accumsan est
                                            in tempus etos ullamcorper sem quam suscipit lacus maecenas tortor.
                                            Suspendisse gravida ornare non mattis velit rutrum modest sed do eiusmod
                                            tempor incididunt ut labore et dolore</p>
                                        <div class="info">
                                            <div class="img">
                                                <img src="./dist/image/474aa8a39f0dd7e198b1ce4cdaf25d4d.jpg" alt="">
                                                <div class="title-info">
                                                    <span>Student</span>
                                                    <h3>Selena Willson</h3>
                                                </div>
                                            </div>
                                            <div class="icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32"
                                                     viewBox="0 0 32 32" fill="none">
                                                    <path d="M8.50398 5.44141C5.92798 5.44141 3.83838 7.59021 3.83838 10.2414C3.83838 12.891 5.92798 15.0414 8.50398 15.0414C13.168 15.0414 10.0592 24.3198 3.83838 24.3198V26.5598C14.9408 26.5614 19.2912 5.44141 8.50398 5.44141ZM21.944 5.44141C19.3696 5.44141 17.28 7.59021 17.28 10.2414C17.28 12.891 19.3696 15.0414 21.944 15.0414C26.6096 15.0414 23.5008 24.3198 17.28 24.3198V26.5598C28.3808 26.5614 32.7312 5.44141 21.944 5.44141Z"
                                                          fill="#41464B"/>
                                                </svg>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <p>We are United CFO, We provide Finance consulting from 30 years. Accumsan est
                                            in tempus etos ullamcorper sem quam suscipit lacus maecenas tortor.
                                            Suspendisse gravida ornare non mattis velit rutrum modest sed do eiusmod
                                            tempor incididunt ut labore et dolore</p>
                                        <div class="info">
                                            <div class="img">
                                                <img src="./dist/image/474aa8a39f0dd7e198b1ce4cdaf25d4d.jpg" alt="">
                                                <div class="title-info">
                                                    <span>Student</span>
                                                    <h3>Selena Willson</h3>
                                                </div>
                                            </div>
                                            <div class="icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32"
                                                     viewBox="0 0 32 32" fill="none">
                                                    <path d="M8.50398 5.44141C5.92798 5.44141 3.83838 7.59021 3.83838 10.2414C3.83838 12.891 5.92798 15.0414 8.50398 15.0414C13.168 15.0414 10.0592 24.3198 3.83838 24.3198V26.5598C14.9408 26.5614 19.2912 5.44141 8.50398 5.44141ZM21.944 5.44141C19.3696 5.44141 17.28 7.59021 17.28 10.2414C17.28 12.891 19.3696 15.0414 21.944 15.0414C26.6096 15.0414 23.5008 24.3198 17.28 24.3198V26.5598C28.3808 26.5614 32.7312 5.44141 21.944 5.44141Z"
                                                          fill="#41464B"/>
                                                </svg>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <p>We are United CFO, We provide Finance consulting from 30 years. Accumsan est
                                            in tempus etos ullamcorper sem quam suscipit lacus maecenas tortor.
                                            Suspendisse gravida ornare non mattis velit rutrum modest sed do eiusmod
                                            tempor incididunt ut labore et dolore</p>
                                        <div class="info">
                                            <div class="img">
                                                <img src="./dist/image/474aa8a39f0dd7e198b1ce4cdaf25d4d.jpg" alt="">
                                                <div class="title-info">
                                                    <span>Student</span>
                                                    <h3>Selena Willson</h3>
                                                </div>
                                            </div>
                                            <div class="icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32"
                                                     viewBox="0 0 32 32" fill="none">
                                                    <path d="M8.50398 5.44141C5.92798 5.44141 3.83838 7.59021 3.83838 10.2414C3.83838 12.891 5.92798 15.0414 8.50398 15.0414C13.168 15.0414 10.0592 24.3198 3.83838 24.3198V26.5598C14.9408 26.5614 19.2912 5.44141 8.50398 5.44141ZM21.944 5.44141C19.3696 5.44141 17.28 7.59021 17.28 10.2414C17.28 12.891 19.3696 15.0414 21.944 15.0414C26.6096 15.0414 23.5008 24.3198 17.28 24.3198V26.5598C28.3808 26.5614 32.7312 5.44141 21.944 5.44141Z"
                                                          fill="#41464B"/>
                                                </svg>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <p>We are United CFO, We provide Finance consulting from 30 years. Accumsan est
                                            in tempus etos ullamcorper sem quam suscipit lacus maecenas tortor.
                                            Suspendisse gravida ornare non mattis velit rutrum modest sed do eiusmod
                                            tempor incididunt ut labore et dolore</p>
                                        <div class="info">
                                            <div class="img">
                                                <img src="./dist/image/474aa8a39f0dd7e198b1ce4cdaf25d4d.jpg" alt="">
                                                <div class="title-info">
                                                    <span>Student</span>
                                                    <h3>Selena Willson</h3>
                                                </div>
                                            </div>
                                            <div class="icon">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32"
                                                     viewBox="0 0 32 32" fill="none">
                                                    <path d="M8.50398 5.44141C5.92798 5.44141 3.83838 7.59021 3.83838 10.2414C3.83838 12.891 5.92798 15.0414 8.50398 15.0414C13.168 15.0414 10.0592 24.3198 3.83838 24.3198V26.5598C14.9408 26.5614 19.2912 5.44141 8.50398 5.44141ZM21.944 5.44141C19.3696 5.44141 17.28 7.59021 17.28 10.2414C17.28 12.891 19.3696 15.0414 21.944 15.0414C26.6096 15.0414 23.5008 24.3198 17.28 24.3198V26.5598C28.3808 26.5614 32.7312 5.44141 21.944 5.44141Z"
                                                          fill="#41464B"/>
                                                </svg>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="swiper-pagination"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
    <section class="section-acdemic">
        <div class="container">
            <div class="note-title">
                <div class="title">
                    <h3>Academic</h3>
                    <p>There are many variations of passages of Lorem Ipsum available, but <br> the majority have
                        suffered alteration in some form</p>
                </div>
                <div class="btn-more">
                    <a href="">Register for consultation
                        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="14" viewBox="0 0 20 14"
                             fill="none">
                            <path d="M12.43 13.82C12.24 13.82 12.05 13.75 11.9 13.6C11.61 13.31 11.61 12.83 11.9 12.54L17.44 7L11.9 1.46C11.61 1.17 11.61 0.689995 11.9 0.399995C12.19 0.109995 12.67 0.109995 12.96 0.399995L19.03 6.47C19.32 6.76 19.32 7.24 19.03 7.52999L12.96 13.6C12.81 13.75 12.62 13.82 12.43 13.82Z"
                                  fill="white"/>
                            <path d="M18.33 7.75H1.5C1.09 7.75 0.75 7.41 0.75 7C0.75 6.59 1.09 6.25 1.5 6.25H18.33C18.74 6.25 19.08 6.59 19.08 7C19.08 7.41 18.74 7.75 18.33 7.75Z"
                                  fill="white"/>
                        </svg>
                    </a>
                </div>
            </div>
            <div class="content">
                <div class="row">
                    <div class="col-md-4 col-6">
                        <div class="item">
                            <div class="img">
                                <figure>
                                    <a href="">
                                        <img src="./dist/image/299ee000f9977657411f921a1f5fd674.png" alt="">
                                    </a>
                                </figure>
                            </div>
                            <div class="content-des">

                                <h3><a href="">Curiosity at Our Heart: Igniting a Lifelong Love for Learning </a></h3>
                                <p>At our Grand Opening, parents and guests were treated to a vibrant physical theatre
                                    performance by our Year 9 Drama pupils. Bending...</p>
                                <span>View detail >></span>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-6">
                        <div class="item">
                            <div class="img">
                                <figure>
                                    <a href="">
                                        <img src="./dist/image/299ee000f9977657411f921a1f5fd674.png" alt="">
                                    </a>
                                </figure>
                            </div>
                            <div class="content-des">

                                <h3><a href="">Curiosity at Our Heart: Igniting a Lifelong Love for Learning </a></h3>
                                <p>At our Grand Opening, parents and guests were treated to a vibrant physical theatre
                                    performance by our Year 9 Drama pupils. Bending...</p>
                                <span>View detail >></span>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-6">
                        <div class="item">
                            <div class="img">
                                <figure>
                                    <a href="">
                                        <img src="./dist/image/299ee000f9977657411f921a1f5fd674.png" alt="">
                                    </a>
                                </figure>
                            </div>
                            <div class="content-des">

                                <h3><a href="">Curiosity at Our Heart: Igniting a Lifelong Love for Learning </a></h3>
                                <p>At our Grand Opening, parents and guests were treated to a vibrant physical theatre
                                    performance by our Year 9 Drama pupils. Bending...</p>
                                <span>View detail >></span>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section-university">
        <div class="container">
            <div class="title">
                <h3>University offer</h3>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </br> Lorem Ipsum has been
                    the industry's standard dummy</p>
            </div>
            <div class="sliderUni swiper">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <div class="img">
                            <img src="./dist/image/02db5dc99e15e6180934f84979735eda.png" alt="">
                        </div>
                        <div class="tit">
                            <h4>King’s College London</h4>
                            <p>Britannia</p>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="img">
                            <img src="./dist/image/8a683c044737cfd8756cc5fe29e3cb50.png" alt="">
                        </div>
                        <div class="tit">
                            <h4>King’s College London</h4>
                            <p>Britannia</p>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="img">
                            <img src="./dist/image/3acef4c52b7854408ad0f62735ffdfc0.png" alt="">
                        </div>
                        <div class="tit">
                            <h4>King’s College London</h4>
                            <p>Britannia</p>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="img">
                            <img src="./dist/image/7e38e2f9779f4ce504232fb02d43f49d.png" alt="">
                        </div>
                        <div class="tit">
                            <h4>King’s College London</h4>
                            <p>Britannia</p>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="img">
                            <img src="./dist/image/a806b7ace6d549d1ca0c618841e61b4e.png" alt="">
                        </div>
                        <div class="tit">
                            <h4>King’s College London</h4>
                            <p>Britannia</p>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="img">
                            <img src="./dist/image/7e38e2f9779f4ce504232fb02d43f49d.png" alt="">
                        </div>
                        <div class="tit">
                            <h4>King’s College London</h4>
                            <p>Britannia</p>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="img">
                            <img src="./dist/image/8a683c044737cfd8756cc5fe29e3cb50.png" alt="">
                        </div>
                        <div class="tit">
                            <h4>King’s College London</h4>
                            <p>Britannia</p>
                        </div>
                    </div>
                </div>
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
            </div>
            <div class="btn-more">
                <a href="">Register for consultation
                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="14" viewBox="0 0 20 14"
                         fill="none">
                        <path d="M12.43 13.82C12.24 13.82 12.05 13.75 11.9 13.6C11.61 13.31 11.61 12.83 11.9 12.54L17.44 7L11.9 1.46C11.61 1.17 11.61 0.689995 11.9 0.399995C12.19 0.109995 12.67 0.109995 12.96 0.399995L19.03 6.47C19.32 6.76 19.32 7.24 19.03 7.52999L12.96 13.6C12.81 13.75 12.62 13.82 12.43 13.82Z"
                              fill="white"/>
                        <path d="M18.33 7.75H1.5C1.09 7.75 0.75 7.41 0.75 7C0.75 6.59 1.09 6.25 1.5 6.25H18.33C18.74 6.25 19.08 6.59 19.08 7C19.08 7.41 18.74 7.75 18.33 7.75Z"
                              fill="white"/>
                    </svg>
                </a>
            </div>
        </div>
    </section>
    <section class="section-ccrediation">
        <div class="container">
            <div class="title">
                <h3>Accrediation & Association</h3>
                <p>We are proud to have created a high-quality learning environment that enables current and future
                    students to </br> flourish and make a difference in the world. Our broad range of programmes is
                    designed to respond to the rapidly </br> evolving education and employment landscape.</p>
            </div>
            <div class="content">
                <div class="row">
                    <div class="col-md-3 col-6">
                        <div class="item">
                            <div class="img">
                                <figure>
                                    <a href="">
                                        <img src="./dist/image/299ee000f9977657411f921a1f5fd674.png" alt="">
                                    </a>
                                </figure>
                            </div>
                            <div class="content-des">

                                <p>At our Grand Opening, parents and guests were treated to a vibrant physical theatre
                                    performance by our Year 9 Drama pupils. Bending...</p>
                                <span>View detail >></span>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-6">
                        <div class="item">
                            <div class="img">
                                <figure>
                                    <a href="">
                                        <img src="./dist/image/299ee000f9977657411f921a1f5fd674.png" alt="">
                                    </a>
                                </figure>
                            </div>
                            <div class="content-des">

                                <p>At our Grand Opening, parents and guests were treated to a vibrant physical theatre
                                    performance by our Year 9 Drama pupils. Bending...</p>
                                <span>View detail >></span>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-6">
                        <div class="item">
                            <div class="img">
                                <figure>
                                    <a href="">
                                        <img src="./dist/image/299ee000f9977657411f921a1f5fd674.png" alt="">
                                    </a>
                                </figure>
                            </div>
                            <div class="content-des">

                                <p>At our Grand Opening, parents and guests were treated to a vibrant physical theatre
                                    performance by our Year 9 Drama pupils. Bending...</p>
                                <span>View detail >></span>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-6">
                        <div class="item">
                            <div class="img">
                                <figure>
                                    <a href="">
                                        <img src="./dist/image/299ee000f9977657411f921a1f5fd674.png" alt="">
                                    </a>
                                </figure>
                            </div>
                            <div class="content-des">

                                <p>At our Grand Opening, parents and guests were treated to a vibrant physical theatre
                                    performance by our Year 9 Drama pupils. Bending...</p>
                                <span>View detail >></span>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
<?php include 'footer.php'; ?>
<script>
    var swiper = new Swiper(".mySwiper", {
        // cssMode: true,
        navigation: {
            nextEl: ".swiper-button-next",
            prevEl: ".swiper-button-prev",
        },
        pagination: {
            el: ".swiper-pagination",
            clickable: true,
        },
        // mousewheel: true,
        // keyboard: true,
    });
    var sliderParents = new Swiper(".sliderParents", {
        // cssMode: true,
        pagination: {
            el: ".swiper-pagination",
            clickable: true,
        },
        // mousewheel: true,
        // keyboard: true,
    });
    var sliderlive = new Swiper(".sliderlive", {
        // cssMode: true,
        pagination: {
            el: ".swiper-pagination",
            clickable: true,
        },
        // mousewheel: true,
        // keyboard: true,
    });
    var sliderUni = new Swiper(".sliderUni", {
        // cssMode: true,
        slidesPerView: 5,
        loop: true,
        navigation: {
            nextEl: ".swiper-button-next",
            prevEl: ".swiper-button-prev",
        },
        // mousewheel: true,
        // keyboard: true,
    });
</script>