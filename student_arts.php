<?php include 'header.php' ?>
<section class="banner">
    <picture>
        <img src="./dist/image/Rectangle 197.png" alt="">
    </picture>
    <div class="text">
        <ul class="text-top">
            <li>
                <a href="#">Home</a>
                <span>></span>
            </li>
            <li>
                <a href="#">About Us</a>
                <span>></span>
            </li>
            <li>
                <a href="#">Mission & Vision</a>
                <span>></span>
            </li>
        </ul>
        <div class="text-bot">
            <h2>Principal’s Greeting</h2>
        </div>
    </div>
</section>
<section class="scholarship-banner">
    <picture>
        <img src="./dist/image/Rectangle 230.png" alt="">
    </picture>
    <div class="container">
        <div class="scholarship-banner-content">
            <span></span>
            <div class="content">
                <h4>KGS International School</h4>
                <div class="text">
                    <span>At the heart of KGS International School's vision and mission is the commitment to create a transformative educational environment. We envision a school where students are not only well-versed in academic excellence but are also equipped with a profound understanding of the world, a global mindset, and a sense of responsibility. Our vision extends beyond conventional educational paradigms; we aim to graduate individuals who, with confidence and resilience, lead the way in navigating the complexities of our ever-evolving global society.</span>
                </div>
            </div>
            <span></span>
        </div>
        <picture>
            <img src="./dist/image/Rectangle 232 (1).png" alt="">
        </picture>
    </div>
</section>
<section class="scholarship-list">
    <div class="container">
        <div class="scholarship-list-item">
            <div class="image">
                <h5>Athletics</h5>
                <picture>
                    <img src="./dist/image/Rectangle 244 (1).png" alt="">
                </picture>
            </div>
            <div class="text">
                <span>Core skills are developed across all disciplines in both track and field. Our athletes enjoy attending inter-school meets and representing the school in a range of events.</span>
            </div>
        </div>
        <div class="scholarship-list-item">
            <div class="image">
                <h5>Football</h5>
                <picture>
                    <img src="./dist/image/Rectangle 244 (1).png" alt="">
                </picture>
            </div>
            <div class="text">
                <span>Boys and girls from Year 1 learn to play a sport that is widely popular around the world through a fantastic range of matches and tournaments. This is a chance for pupils to not only improve their fitness, but to also to learn about teamwork and cooperation.</span>
            </div>
        </div>
        <div class="scholarship-list-item">
            <div class="image">
                <h5>Water Polo</h5>
                <picture>
                    <img src="./dist/image/Rectangle 244 (1).png" alt="">
                </picture>
            </div>
            <div class="text">
                <span>5 top pupils enrolled in Year 10 in 2023 will be awarded a scholarship worth 100% tuition fee reduction. This scholarship will be granted (with conditions) for the duration of the pupil’s career at Brighton College Vietnam.</span>
            </div>
        </div>
        <div class="scholarship-list-item">
            <div class="image">
                <h5>Water Polo</h5>
                <picture>
                    <img src="./dist/image/Rectangle 244 (1).png" alt="">
                </picture>
            </div>
            <div class="text">
                <span>5 top pupils enrolled in Year 10 in 2023 will be awarded a scholarship worth 100% tuition fee reduction. This scholarship will be granted (with conditions) for the duration of the pupil’s career at Brighton College Vietnam.</span>
            </div>
        </div>
        <div class="scholarship-list-item">
            <div class="image">
                <h5>Water Polo</h5>
                <picture>
                    <img src="./dist/image/Rectangle 244 (1).png" alt="">
                </picture>
            </div>
            <div class="text">
                <span>5 top pupils enrolled in Year 10 in 2023 will be awarded a scholarship worth 100% tuition fee reduction. This scholarship will be granted (with conditions) for the duration of the pupil’s career at Brighton College Vietnam.</span>
            </div>
        </div>
    </div>
</section>
<?php include 'footer.php' ?>