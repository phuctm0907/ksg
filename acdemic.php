<?php include 'header.php' ?>
    <section class="banner">
        <picture>
            <img src="./dist/image/Rectangle 197.png" alt="">
        </picture>
        <div class="text">
            <ul class="text-top">
                <li>
                    <a href="#">Home</a>
                    <span>></span>
                </li>
                <li>
                    <a href="#">Academic</a>
                </li>
            </ul>
            <div class="text-bot">
                <h2>Academic</h2>
            </div>
        </div>
    </section>
    <section class="about-academic" style="background-image: url(./dist/image/Rectangles.png);">
        <div class="container">
            <div class="about">
                <div class="row flex-note">
                    <div class="col-md-6">
                        <div class="content">
                            <h3>KGS International School</h3>
                            <p>At the heart of KGS International School's vision and mission is the commitment to create
                                a transformative educational environment. We envision a school where students are not
                                only well-versed in academic excellence but are also equipped with a profound
                                understanding of the world, a global mindset, and a sense of responsibility. Our vision
                                extends beyond conventional educational paradigms; we aim to graduate individuals who,
                                with confidence and resilience, lead the way in navigating the complexities of our
                                ever-evolving global society.</p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="img">
                            <img src="./dist/image/2a37bad004b6ef87287ba3c53420c5a0.png" alt="">
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
    <div class="section-curriculum">
        <div class="container">
            <h3>Curriculum</h3>
            <ul class="nav nav-pills nav-tabcontent" id="pills-tab" role="tablist">
                <li class="nav-item" role="presentation">
                    <button class="nav-link active" id="pills-home-tab" data-bs-toggle="pill"
                            data-bs-target="#pills-home" type="button" role="tab" aria-controls="pills-home"
                            aria-selected="true">Home
                    </button>
                </li>
                <li class="nav-item" role="presentation">
                    <button class="nav-link" id="pills-profile-tab" data-bs-toggle="pill"
                            data-bs-target="#pills-profile" type="button" role="tab" aria-controls="pills-profile"
                            aria-selected="false">Profile
                    </button>
                </li>
                <li class="nav-item" role="presentation">
                    <button class="nav-link" id="pills-contact-tab" data-bs-toggle="pill"
                            data-bs-target="#pills-contact" type="button" role="tab" aria-controls="pills-contact"
                            aria-selected="false">Contact
                    </button>
                </li>
            </ul>
            <div class="tab-content" id="pills-tabContent">
                <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                    <div class="content">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="img-about">
                                    <img class="img-1" src="./dist/image/607420e040a6a286979b8f08fba151fc.png" alt="">
                                    <img class="img-2" src="./dist/image/a96288cab0e875583663ca270ad82843.png" alt="">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="content-des">
                                    <div class="title">
                                        <h3>A school of DREAM</h3>
                                        <p>We are proud to be a part of the KGS family. In their Independent Schools
                                            Guide,
                                            widely acknowledged as the most authoritative survey of the top schools in
                                            the
                                            Korean, The Sunday Times names the College as “one of the hottest tickets in
                                            independent education.”</p>
                                        <p>We are proud to be a part of the KGS family. In their Independent Schools
                                            Guide,
                                            widely acknowledged as the most authoritative survey of the top schools in
                                            the
                                            Korean, The Sunday Times names the College as “one of the hottest tickets in
                                            independent education.”</p>
                                    </div>
                                    <div class="btn-more">
                                        <a href="">View more detail
                                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="14"
                                                 viewBox="0 0 20 14"
                                                 fill="none">
                                                <path d="M12.43 13.82C12.24 13.82 12.05 13.75 11.9 13.6C11.61 13.31 11.61 12.83 11.9 12.54L17.44 7L11.9 1.46C11.61 1.17 11.61 0.689995 11.9 0.399995C12.19 0.109995 12.67 0.109995 12.96 0.399995L19.03 6.47C19.32 6.76 19.32 7.24 19.03 7.52999L12.96 13.6C12.81 13.75 12.62 13.82 12.43 13.82Z"
                                                      fill="white"/>
                                                <path d="M18.33 7.75H1.5C1.09 7.75 0.75 7.41 0.75 7C0.75 6.59 1.09 6.25 1.5 6.25H18.33C18.74 6.25 19.08 6.59 19.08 7C19.08 7.41 18.74 7.75 18.33 7.75Z"
                                                      fill="white"/>
                                            </svg>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                    <div class="content">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="img-about">
                                    <img class="img-1" src="./dist/image/607420e040a6a286979b8f08fba151fc.png" alt="">
                                    <img class="img-2" src="./dist/image/a96288cab0e875583663ca270ad82843.png" alt="">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="content-des">
                                    <div class="title">
                                        <h3>A school of DREAM</h3>
                                        <p>We are proud to be a part of the KGS family. In their Independent Schools
                                            Guide,
                                            widely acknowledged as the most authoritative survey of the top schools in
                                            the
                                            Korean, The Sunday Times names the College as “one of the hottest tickets in
                                            independent education.”</p>
                                        <p>We are proud to be a part of the KGS family. In their Independent Schools
                                            Guide,
                                            widely acknowledged as the most authoritative survey of the top schools in
                                            the
                                            Korean, The Sunday Times names the College as “one of the hottest tickets in
                                            independent education.”</p>
                                    </div>
                                    <div class="btn-more">
                                        <a href="">View more detail
                                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="14"
                                                 viewBox="0 0 20 14"
                                                 fill="none">
                                                <path d="M12.43 13.82C12.24 13.82 12.05 13.75 11.9 13.6C11.61 13.31 11.61 12.83 11.9 12.54L17.44 7L11.9 1.46C11.61 1.17 11.61 0.689995 11.9 0.399995C12.19 0.109995 12.67 0.109995 12.96 0.399995L19.03 6.47C19.32 6.76 19.32 7.24 19.03 7.52999L12.96 13.6C12.81 13.75 12.62 13.82 12.43 13.82Z"
                                                      fill="white"/>
                                                <path d="M18.33 7.75H1.5C1.09 7.75 0.75 7.41 0.75 7C0.75 6.59 1.09 6.25 1.5 6.25H18.33C18.74 6.25 19.08 6.59 19.08 7C19.08 7.41 18.74 7.75 18.33 7.75Z"
                                                      fill="white"/>
                                            </svg>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
                    <div class="content">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="img-about">
                                    <img class="img-1" src="./dist/image/607420e040a6a286979b8f08fba151fc.png" alt="">
                                    <img class="img-2" src="./dist/image/a96288cab0e875583663ca270ad82843.png" alt="">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="content-des">
                                    <div class="title">
                                        <h3>A school of DREAM</h3>
                                        <p>We are proud to be a part of the KGS family. In their Independent Schools
                                            Guide,
                                            widely acknowledged as the most authoritative survey of the top schools in
                                            the
                                            Korean, The Sunday Times names the College as “one of the hottest tickets in
                                            independent education.”</p>
                                        <p>We are proud to be a part of the KGS family. In their Independent Schools
                                            Guide,
                                            widely acknowledged as the most authoritative survey of the top schools in
                                            the
                                            Korean, The Sunday Times names the College as “one of the hottest tickets in
                                            independent education.”</p>
                                    </div>
                                    <div class="btn-more">
                                        <a href="">View more detail
                                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="14"
                                                 viewBox="0 0 20 14"
                                                 fill="none">
                                                <path d="M12.43 13.82C12.24 13.82 12.05 13.75 11.9 13.6C11.61 13.31 11.61 12.83 11.9 12.54L17.44 7L11.9 1.46C11.61 1.17 11.61 0.689995 11.9 0.399995C12.19 0.109995 12.67 0.109995 12.96 0.399995L19.03 6.47C19.32 6.76 19.32 7.24 19.03 7.52999L12.96 13.6C12.81 13.75 12.62 13.82 12.43 13.82Z"
                                                      fill="white"/>
                                                <path d="M18.33 7.75H1.5C1.09 7.75 0.75 7.41 0.75 7C0.75 6.59 1.09 6.25 1.5 6.25H18.33C18.74 6.25 19.08 6.59 19.08 7C19.08 7.41 18.74 7.75 18.33 7.75Z"
                                                      fill="white"/>
                                            </svg>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<section class="section-edu">
    <div class="container">
        <div class="title">
            <h3>Educational philosophy & methods</h3>
        </div>
      <div class="todo-edu">
          <div class="row ">
              <div class="col-md-6">
                  <div class="item-edu">
                      <div class="cont">
                          <h4>Primary</h4>
                          <p>We are proud to be a part of the KGS family. In their Independent Schools Guide, widely acknowledged as the most authoritative</p>
                      </div>
                      <div class="img">
                          <svg xmlns="http://www.w3.org/2000/svg" width="64" height="64" viewBox="0 0 64 64" fill="none">
                              <path d="M26.5996 60.7921C25.8902 60.7921 25.2236 60.5174 24.7196 60.0161C24.1329 59.4348 23.8529 58.6294 23.9516 57.8081L24.8769 49.9094C24.8609 49.9094 24.8449 49.9094 24.8316 49.9094C24.4742 49.9094 24.1409 49.7708 23.8876 49.5201L14.5009 40.1414C14.1969 39.8401 14.0716 39.4268 14.1222 39.0294L6.20756 40.0001C6.09822 40.0134 5.98622 40.0214 5.87689 40.0214C5.16756 40.0214 4.50089 39.7441 3.99422 39.2401C3.49022 38.7361 3.21289 38.0668 3.21289 37.3521C3.21289 36.6401 3.49022 35.9708 3.99422 35.4668L10.6876 28.7761C12.4076 27.0321 14.4209 25.6401 16.6689 24.6454C16.7649 24.6028 16.8689 24.5708 16.9729 24.5521C20.3916 23.9468 23.8982 23.6401 27.3942 23.6401C27.9462 23.6401 28.4956 23.6481 29.0476 23.6614C36.6689 16.0188 44.6209 8.04275 45.5729 7.09342C47.7622 4.90142 53.7889 3.32009 58.4609 2.37609C58.6342 2.34142 58.8102 2.32275 58.9862 2.32275C60.2502 2.32275 61.3489 3.22142 61.6022 4.46142C61.6716 4.81342 61.6716 5.16809 61.6022 5.51742C60.6582 10.1868 59.0769 16.2134 56.8849 18.4054L40.3142 34.9734C40.3942 38.9894 40.0662 43.0161 39.3409 46.9441C39.3222 47.0428 39.2956 47.1334 39.2582 47.2214C38.2982 49.4934 36.9356 51.5361 35.2049 53.2961L28.4849 60.0054C27.9836 60.5068 27.3169 60.7868 26.6049 60.7868C26.6049 60.7921 26.5996 60.7921 26.5996 60.7921ZM26.5996 58.1228L33.3142 51.4214C34.7649 49.9441 35.9222 48.2268 36.7489 46.3201C37.2689 43.4694 37.5702 40.5601 37.6449 37.6508L29.5942 45.7014C29.5836 45.7121 29.5756 45.7254 29.5649 45.7334C29.5436 45.7548 29.5196 45.7788 29.4956 45.7974L27.8476 47.4428L26.5996 58.1228ZM24.8289 46.6934L26.7169 44.8054L19.2182 37.3014L17.3249 39.2001L24.8289 46.6934ZM28.6049 42.9201L55.0022 16.5254C56.3062 15.2214 57.7969 10.9121 58.9889 4.99742C53.0716 6.19209 48.7649 7.68009 47.4609 8.98409C45.9702 10.4721 27.5009 28.9948 21.1009 35.4134L28.6049 42.9201ZM17.6049 27.1521C15.7222 28.0054 14.0342 29.1841 12.5836 30.6561L5.88222 37.3548L16.7222 36.0294L18.1409 34.6054C18.1622 34.5708 18.2022 34.5121 18.2636 34.4534C18.3169 34.4001 18.3702 34.3628 18.4102 34.3388L26.4102 26.3148C23.4636 26.3654 20.5062 26.6454 17.6049 27.1521Z" fill="url(#paint0_linear_1904_7431)"/>
                              <path d="M5.021 61.6639C4.55166 61.6639 4.08766 61.5386 3.67966 61.2986C3.06366 60.9386 2.62633 60.3599 2.44766 59.6719C2.333 59.2346 2.333 58.7732 2.445 58.3332C3.81566 53.2719 5.86633 46.8266 7.85833 44.8212C9.42366 43.2586 11.205 42.3999 12.8797 42.3999C14.1837 42.3999 15.3837 42.9092 16.3517 43.8772C16.6023 44.1279 16.741 44.4639 16.741 44.8212C16.741 45.1786 16.6023 45.5119 16.3517 45.7652C16.101 46.0159 15.765 46.1546 15.4103 46.1546C15.0557 46.1546 14.7197 46.0159 14.4663 45.7652C13.9997 45.2986 13.4823 45.0719 12.885 45.0719C11.9303 45.0719 10.789 45.6666 9.749 46.7066C8.82633 47.6346 7.101 51.3572 5.02633 59.0159C12.653 56.9066 16.365 55.1679 17.2957 54.2372C18.1517 53.3812 18.7117 52.4346 18.8717 51.5759C19.021 50.7706 18.813 50.0986 18.237 49.5226C17.717 49.0026 17.717 48.1572 18.2343 47.6372C18.485 47.3839 18.821 47.2452 19.1783 47.2452C19.533 47.2452 19.869 47.3839 20.1197 47.6346C21.333 48.8452 21.8077 50.3786 21.493 52.0639C21.2263 53.4666 20.4263 54.8692 19.1757 56.1226C17.173 58.1226 10.7517 60.1919 5.717 61.5732C5.485 61.6346 5.253 61.6639 5.021 61.6639Z" fill="url(#paint1_linear_1904_7431)"/>
                              <defs>
                                  <linearGradient id="paint0_linear_1904_7431" x1="3.21289" y1="33.5064" x2="61.6542" y2="33.5064" gradientUnits="userSpaceOnUse">
                                      <stop stop-color="#0D487A"/>
                                      <stop offset="1" stop-color="#0089C3"/>
                                  </linearGradient>
                                  <linearGradient id="paint1_linear_1904_7431" x1="2.36133" y1="52.674" x2="21.5875" y2="52.674" gradientUnits="userSpaceOnUse">
                                      <stop stop-color="#0D487A"/>
                                      <stop offset="1" stop-color="#0089C3"/>
                                  </linearGradient>
                              </defs>
                          </svg>
                      </div>
                  </div>
              </div>
              <div class="col-md-6">
                  <div class="item-edu">
                      <div class="cont">
                          <h4>Primary</h4>
                          <p>We are proud to be a part of the KGS family. In their Independent Schools Guide, widely acknowledged as the most authoritative</p>
                      </div>
                      <div class="img">
                          <svg xmlns="http://www.w3.org/2000/svg" width="64" height="64" viewBox="0 0 64 64" fill="none">
                              <path d="M26.5996 60.7921C25.8902 60.7921 25.2236 60.5174 24.7196 60.0161C24.1329 59.4348 23.8529 58.6294 23.9516 57.8081L24.8769 49.9094C24.8609 49.9094 24.8449 49.9094 24.8316 49.9094C24.4742 49.9094 24.1409 49.7708 23.8876 49.5201L14.5009 40.1414C14.1969 39.8401 14.0716 39.4268 14.1222 39.0294L6.20756 40.0001C6.09822 40.0134 5.98622 40.0214 5.87689 40.0214C5.16756 40.0214 4.50089 39.7441 3.99422 39.2401C3.49022 38.7361 3.21289 38.0668 3.21289 37.3521C3.21289 36.6401 3.49022 35.9708 3.99422 35.4668L10.6876 28.7761C12.4076 27.0321 14.4209 25.6401 16.6689 24.6454C16.7649 24.6028 16.8689 24.5708 16.9729 24.5521C20.3916 23.9468 23.8982 23.6401 27.3942 23.6401C27.9462 23.6401 28.4956 23.6481 29.0476 23.6614C36.6689 16.0188 44.6209 8.04275 45.5729 7.09342C47.7622 4.90142 53.7889 3.32009 58.4609 2.37609C58.6342 2.34142 58.8102 2.32275 58.9862 2.32275C60.2502 2.32275 61.3489 3.22142 61.6022 4.46142C61.6716 4.81342 61.6716 5.16809 61.6022 5.51742C60.6582 10.1868 59.0769 16.2134 56.8849 18.4054L40.3142 34.9734C40.3942 38.9894 40.0662 43.0161 39.3409 46.9441C39.3222 47.0428 39.2956 47.1334 39.2582 47.2214C38.2982 49.4934 36.9356 51.5361 35.2049 53.2961L28.4849 60.0054C27.9836 60.5068 27.3169 60.7868 26.6049 60.7868C26.6049 60.7921 26.5996 60.7921 26.5996 60.7921ZM26.5996 58.1228L33.3142 51.4214C34.7649 49.9441 35.9222 48.2268 36.7489 46.3201C37.2689 43.4694 37.5702 40.5601 37.6449 37.6508L29.5942 45.7014C29.5836 45.7121 29.5756 45.7254 29.5649 45.7334C29.5436 45.7548 29.5196 45.7788 29.4956 45.7974L27.8476 47.4428L26.5996 58.1228ZM24.8289 46.6934L26.7169 44.8054L19.2182 37.3014L17.3249 39.2001L24.8289 46.6934ZM28.6049 42.9201L55.0022 16.5254C56.3062 15.2214 57.7969 10.9121 58.9889 4.99742C53.0716 6.19209 48.7649 7.68009 47.4609 8.98409C45.9702 10.4721 27.5009 28.9948 21.1009 35.4134L28.6049 42.9201ZM17.6049 27.1521C15.7222 28.0054 14.0342 29.1841 12.5836 30.6561L5.88222 37.3548L16.7222 36.0294L18.1409 34.6054C18.1622 34.5708 18.2022 34.5121 18.2636 34.4534C18.3169 34.4001 18.3702 34.3628 18.4102 34.3388L26.4102 26.3148C23.4636 26.3654 20.5062 26.6454 17.6049 27.1521Z" fill="url(#paint0_linear_1904_7431)"/>
                              <path d="M5.021 61.6639C4.55166 61.6639 4.08766 61.5386 3.67966 61.2986C3.06366 60.9386 2.62633 60.3599 2.44766 59.6719C2.333 59.2346 2.333 58.7732 2.445 58.3332C3.81566 53.2719 5.86633 46.8266 7.85833 44.8212C9.42366 43.2586 11.205 42.3999 12.8797 42.3999C14.1837 42.3999 15.3837 42.9092 16.3517 43.8772C16.6023 44.1279 16.741 44.4639 16.741 44.8212C16.741 45.1786 16.6023 45.5119 16.3517 45.7652C16.101 46.0159 15.765 46.1546 15.4103 46.1546C15.0557 46.1546 14.7197 46.0159 14.4663 45.7652C13.9997 45.2986 13.4823 45.0719 12.885 45.0719C11.9303 45.0719 10.789 45.6666 9.749 46.7066C8.82633 47.6346 7.101 51.3572 5.02633 59.0159C12.653 56.9066 16.365 55.1679 17.2957 54.2372C18.1517 53.3812 18.7117 52.4346 18.8717 51.5759C19.021 50.7706 18.813 50.0986 18.237 49.5226C17.717 49.0026 17.717 48.1572 18.2343 47.6372C18.485 47.3839 18.821 47.2452 19.1783 47.2452C19.533 47.2452 19.869 47.3839 20.1197 47.6346C21.333 48.8452 21.8077 50.3786 21.493 52.0639C21.2263 53.4666 20.4263 54.8692 19.1757 56.1226C17.173 58.1226 10.7517 60.1919 5.717 61.5732C5.485 61.6346 5.253 61.6639 5.021 61.6639Z" fill="url(#paint1_linear_1904_7431)"/>
                              <defs>
                                  <linearGradient id="paint0_linear_1904_7431" x1="3.21289" y1="33.5064" x2="61.6542" y2="33.5064" gradientUnits="userSpaceOnUse">
                                      <stop stop-color="#0D487A"/>
                                      <stop offset="1" stop-color="#0089C3"/>
                                  </linearGradient>
                                  <linearGradient id="paint1_linear_1904_7431" x1="2.36133" y1="52.674" x2="21.5875" y2="52.674" gradientUnits="userSpaceOnUse">
                                      <stop stop-color="#0D487A"/>
                                      <stop offset="1" stop-color="#0089C3"/>
                                  </linearGradient>
                              </defs>
                          </svg>
                      </div>
                  </div>
              </div>
              <div class="col-md-6">
                  <div class="item-edu">
                      <div class="cont">
                          <h4>Primary</h4>
                          <p>We are proud to be a part of the KGS family. In their Independent Schools Guide, widely acknowledged as the most authoritative</p>
                      </div>
                      <div class="img">
                          <svg xmlns="http://www.w3.org/2000/svg" width="64" height="64" viewBox="0 0 64 64" fill="none">
                              <path d="M26.5996 60.7921C25.8902 60.7921 25.2236 60.5174 24.7196 60.0161C24.1329 59.4348 23.8529 58.6294 23.9516 57.8081L24.8769 49.9094C24.8609 49.9094 24.8449 49.9094 24.8316 49.9094C24.4742 49.9094 24.1409 49.7708 23.8876 49.5201L14.5009 40.1414C14.1969 39.8401 14.0716 39.4268 14.1222 39.0294L6.20756 40.0001C6.09822 40.0134 5.98622 40.0214 5.87689 40.0214C5.16756 40.0214 4.50089 39.7441 3.99422 39.2401C3.49022 38.7361 3.21289 38.0668 3.21289 37.3521C3.21289 36.6401 3.49022 35.9708 3.99422 35.4668L10.6876 28.7761C12.4076 27.0321 14.4209 25.6401 16.6689 24.6454C16.7649 24.6028 16.8689 24.5708 16.9729 24.5521C20.3916 23.9468 23.8982 23.6401 27.3942 23.6401C27.9462 23.6401 28.4956 23.6481 29.0476 23.6614C36.6689 16.0188 44.6209 8.04275 45.5729 7.09342C47.7622 4.90142 53.7889 3.32009 58.4609 2.37609C58.6342 2.34142 58.8102 2.32275 58.9862 2.32275C60.2502 2.32275 61.3489 3.22142 61.6022 4.46142C61.6716 4.81342 61.6716 5.16809 61.6022 5.51742C60.6582 10.1868 59.0769 16.2134 56.8849 18.4054L40.3142 34.9734C40.3942 38.9894 40.0662 43.0161 39.3409 46.9441C39.3222 47.0428 39.2956 47.1334 39.2582 47.2214C38.2982 49.4934 36.9356 51.5361 35.2049 53.2961L28.4849 60.0054C27.9836 60.5068 27.3169 60.7868 26.6049 60.7868C26.6049 60.7921 26.5996 60.7921 26.5996 60.7921ZM26.5996 58.1228L33.3142 51.4214C34.7649 49.9441 35.9222 48.2268 36.7489 46.3201C37.2689 43.4694 37.5702 40.5601 37.6449 37.6508L29.5942 45.7014C29.5836 45.7121 29.5756 45.7254 29.5649 45.7334C29.5436 45.7548 29.5196 45.7788 29.4956 45.7974L27.8476 47.4428L26.5996 58.1228ZM24.8289 46.6934L26.7169 44.8054L19.2182 37.3014L17.3249 39.2001L24.8289 46.6934ZM28.6049 42.9201L55.0022 16.5254C56.3062 15.2214 57.7969 10.9121 58.9889 4.99742C53.0716 6.19209 48.7649 7.68009 47.4609 8.98409C45.9702 10.4721 27.5009 28.9948 21.1009 35.4134L28.6049 42.9201ZM17.6049 27.1521C15.7222 28.0054 14.0342 29.1841 12.5836 30.6561L5.88222 37.3548L16.7222 36.0294L18.1409 34.6054C18.1622 34.5708 18.2022 34.5121 18.2636 34.4534C18.3169 34.4001 18.3702 34.3628 18.4102 34.3388L26.4102 26.3148C23.4636 26.3654 20.5062 26.6454 17.6049 27.1521Z" fill="url(#paint0_linear_1904_7431)"/>
                              <path d="M5.021 61.6639C4.55166 61.6639 4.08766 61.5386 3.67966 61.2986C3.06366 60.9386 2.62633 60.3599 2.44766 59.6719C2.333 59.2346 2.333 58.7732 2.445 58.3332C3.81566 53.2719 5.86633 46.8266 7.85833 44.8212C9.42366 43.2586 11.205 42.3999 12.8797 42.3999C14.1837 42.3999 15.3837 42.9092 16.3517 43.8772C16.6023 44.1279 16.741 44.4639 16.741 44.8212C16.741 45.1786 16.6023 45.5119 16.3517 45.7652C16.101 46.0159 15.765 46.1546 15.4103 46.1546C15.0557 46.1546 14.7197 46.0159 14.4663 45.7652C13.9997 45.2986 13.4823 45.0719 12.885 45.0719C11.9303 45.0719 10.789 45.6666 9.749 46.7066C8.82633 47.6346 7.101 51.3572 5.02633 59.0159C12.653 56.9066 16.365 55.1679 17.2957 54.2372C18.1517 53.3812 18.7117 52.4346 18.8717 51.5759C19.021 50.7706 18.813 50.0986 18.237 49.5226C17.717 49.0026 17.717 48.1572 18.2343 47.6372C18.485 47.3839 18.821 47.2452 19.1783 47.2452C19.533 47.2452 19.869 47.3839 20.1197 47.6346C21.333 48.8452 21.8077 50.3786 21.493 52.0639C21.2263 53.4666 20.4263 54.8692 19.1757 56.1226C17.173 58.1226 10.7517 60.1919 5.717 61.5732C5.485 61.6346 5.253 61.6639 5.021 61.6639Z" fill="url(#paint1_linear_1904_7431)"/>
                              <defs>
                                  <linearGradient id="paint0_linear_1904_7431" x1="3.21289" y1="33.5064" x2="61.6542" y2="33.5064" gradientUnits="userSpaceOnUse">
                                      <stop stop-color="#0D487A"/>
                                      <stop offset="1" stop-color="#0089C3"/>
                                  </linearGradient>
                                  <linearGradient id="paint1_linear_1904_7431" x1="2.36133" y1="52.674" x2="21.5875" y2="52.674" gradientUnits="userSpaceOnUse">
                                      <stop stop-color="#0D487A"/>
                                      <stop offset="1" stop-color="#0089C3"/>
                                  </linearGradient>
                              </defs>
                          </svg>
                      </div>
                  </div>
              </div>
              <div class="col-md-6">
                  <div class="item-edu">
                      <div class="cont">
                          <h4>Primary</h4>
                          <p>We are proud to be a part of the KGS family. In their Independent Schools Guide, widely acknowledged as the most authoritative</p>
                      </div>
                      <div class="img">
                          <svg xmlns="http://www.w3.org/2000/svg" width="64" height="64" viewBox="0 0 64 64" fill="none">
                              <path d="M26.5996 60.7921C25.8902 60.7921 25.2236 60.5174 24.7196 60.0161C24.1329 59.4348 23.8529 58.6294 23.9516 57.8081L24.8769 49.9094C24.8609 49.9094 24.8449 49.9094 24.8316 49.9094C24.4742 49.9094 24.1409 49.7708 23.8876 49.5201L14.5009 40.1414C14.1969 39.8401 14.0716 39.4268 14.1222 39.0294L6.20756 40.0001C6.09822 40.0134 5.98622 40.0214 5.87689 40.0214C5.16756 40.0214 4.50089 39.7441 3.99422 39.2401C3.49022 38.7361 3.21289 38.0668 3.21289 37.3521C3.21289 36.6401 3.49022 35.9708 3.99422 35.4668L10.6876 28.7761C12.4076 27.0321 14.4209 25.6401 16.6689 24.6454C16.7649 24.6028 16.8689 24.5708 16.9729 24.5521C20.3916 23.9468 23.8982 23.6401 27.3942 23.6401C27.9462 23.6401 28.4956 23.6481 29.0476 23.6614C36.6689 16.0188 44.6209 8.04275 45.5729 7.09342C47.7622 4.90142 53.7889 3.32009 58.4609 2.37609C58.6342 2.34142 58.8102 2.32275 58.9862 2.32275C60.2502 2.32275 61.3489 3.22142 61.6022 4.46142C61.6716 4.81342 61.6716 5.16809 61.6022 5.51742C60.6582 10.1868 59.0769 16.2134 56.8849 18.4054L40.3142 34.9734C40.3942 38.9894 40.0662 43.0161 39.3409 46.9441C39.3222 47.0428 39.2956 47.1334 39.2582 47.2214C38.2982 49.4934 36.9356 51.5361 35.2049 53.2961L28.4849 60.0054C27.9836 60.5068 27.3169 60.7868 26.6049 60.7868C26.6049 60.7921 26.5996 60.7921 26.5996 60.7921ZM26.5996 58.1228L33.3142 51.4214C34.7649 49.9441 35.9222 48.2268 36.7489 46.3201C37.2689 43.4694 37.5702 40.5601 37.6449 37.6508L29.5942 45.7014C29.5836 45.7121 29.5756 45.7254 29.5649 45.7334C29.5436 45.7548 29.5196 45.7788 29.4956 45.7974L27.8476 47.4428L26.5996 58.1228ZM24.8289 46.6934L26.7169 44.8054L19.2182 37.3014L17.3249 39.2001L24.8289 46.6934ZM28.6049 42.9201L55.0022 16.5254C56.3062 15.2214 57.7969 10.9121 58.9889 4.99742C53.0716 6.19209 48.7649 7.68009 47.4609 8.98409C45.9702 10.4721 27.5009 28.9948 21.1009 35.4134L28.6049 42.9201ZM17.6049 27.1521C15.7222 28.0054 14.0342 29.1841 12.5836 30.6561L5.88222 37.3548L16.7222 36.0294L18.1409 34.6054C18.1622 34.5708 18.2022 34.5121 18.2636 34.4534C18.3169 34.4001 18.3702 34.3628 18.4102 34.3388L26.4102 26.3148C23.4636 26.3654 20.5062 26.6454 17.6049 27.1521Z" fill="url(#paint0_linear_1904_7431)"/>
                              <path d="M5.021 61.6639C4.55166 61.6639 4.08766 61.5386 3.67966 61.2986C3.06366 60.9386 2.62633 60.3599 2.44766 59.6719C2.333 59.2346 2.333 58.7732 2.445 58.3332C3.81566 53.2719 5.86633 46.8266 7.85833 44.8212C9.42366 43.2586 11.205 42.3999 12.8797 42.3999C14.1837 42.3999 15.3837 42.9092 16.3517 43.8772C16.6023 44.1279 16.741 44.4639 16.741 44.8212C16.741 45.1786 16.6023 45.5119 16.3517 45.7652C16.101 46.0159 15.765 46.1546 15.4103 46.1546C15.0557 46.1546 14.7197 46.0159 14.4663 45.7652C13.9997 45.2986 13.4823 45.0719 12.885 45.0719C11.9303 45.0719 10.789 45.6666 9.749 46.7066C8.82633 47.6346 7.101 51.3572 5.02633 59.0159C12.653 56.9066 16.365 55.1679 17.2957 54.2372C18.1517 53.3812 18.7117 52.4346 18.8717 51.5759C19.021 50.7706 18.813 50.0986 18.237 49.5226C17.717 49.0026 17.717 48.1572 18.2343 47.6372C18.485 47.3839 18.821 47.2452 19.1783 47.2452C19.533 47.2452 19.869 47.3839 20.1197 47.6346C21.333 48.8452 21.8077 50.3786 21.493 52.0639C21.2263 53.4666 20.4263 54.8692 19.1757 56.1226C17.173 58.1226 10.7517 60.1919 5.717 61.5732C5.485 61.6346 5.253 61.6639 5.021 61.6639Z" fill="url(#paint1_linear_1904_7431)"/>
                              <defs>
                                  <linearGradient id="paint0_linear_1904_7431" x1="3.21289" y1="33.5064" x2="61.6542" y2="33.5064" gradientUnits="userSpaceOnUse">
                                      <stop stop-color="#0D487A"/>
                                      <stop offset="1" stop-color="#0089C3"/>
                                  </linearGradient>
                                  <linearGradient id="paint1_linear_1904_7431" x1="2.36133" y1="52.674" x2="21.5875" y2="52.674" gradientUnits="userSpaceOnUse">
                                      <stop stop-color="#0D487A"/>
                                      <stop offset="1" stop-color="#0089C3"/>
                                  </linearGradient>
                              </defs>
                          </svg>
                      </div>
                  </div>
              </div>
          </div>
      </div>
    </div>
</section>
    <script src="./dist/js/ad_regulation-process.js"></script>
<?php include 'footer.php' ?>