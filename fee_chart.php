<?php include 'header.php' ?>
<section class="banner">
    <picture>
        <img src="./dist/image/Rectangle 197.png" alt="">
    </picture>
    <div class="text">
        <ul class="text-top">
            <li>
                <a href="#">Home</a>
                <span>></span>
            </li>
            <li>
                <a href="#">About Us</a>
                <span>></span>
            </li>
            <li>
                <a href="#">Mission & Vision</a>
                <span>></span>
            </li>
        </ul>
        <div class="text-bot">
            <h2>Principal’s Greeting</h2>
        </div>
    </div>
</section>
<section class="fee-chart">
    <div class="container">
        <div class="fee-chart-title">
            <h3>Tuition</h3>
        </div>
        <div class="fee-chart-content">
            <div class="title">
                <h5>Tuition fee</h5>
            </div>
            <div class="fee-chart-content-info">
                <div class="content-item">
                    <h5>Lorem ipsum</h5>
                    <div class="content-item-text">
                        <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Pellentesque habitant morbi tristique senectus. Arcu ac tortor dignissim convallis. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Pellentesque habitant morbi tristique senectus. Arcu ac tortor dignissim convallis. </span>
                    </div>
                </div>
                <div class="content-item">
                    <h5>Lorem ipsum</h5>
                    <div class="content-item-text">
                        <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Pellentesque habitant morbi tristique senectus. Arcu ac tortor dignissim convallis. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Pellentesque habitant morbi tristique senectus. Arcu ac tortor dignissim convallis. </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="fee-chart-content">
            <div class="title">
                <h5>Late enrolment</h5>
            </div>
            <div class="fee-chart-content-list">
                <div class="top">
                    <div class="top-text">
                        <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Pellentesque habitant morbi tristique senectus. Arcu ac tortor dignissim convallis. </span>
                    </div>
                    <div class="top-button">
                        <a class="top-button-detail" href="#">View details</a>
                        <a class="top-button-download" href="#">
                            <div class="text">
                                <span>Download fee chart</span>
                            </div>
                            <picture>
                            <svg xmlns="http://www.w3.org/2000/svg" width="18" height="16" viewBox="0 0 18 16" fill="none">
                                <path fill-rule="evenodd" clip-rule="evenodd" d="M9 11.7843C8.655 11.7843 8.375 11.5043 8.375 11.1593V1.12512C8.375 0.780122 8.655 0.500122 9 0.500122C9.345 0.500122 9.625 0.780122 9.625 1.12512V11.1593C9.625 11.5043 9.345 11.7843 9 11.7843Z" fill="white"/>
                                <path fill-rule="evenodd" clip-rule="evenodd" d="M8.99979 11.7843C8.83396 11.7843 8.67396 11.7184 8.55729 11.6001L6.12729 9.16094C5.88396 8.91594 5.88479 8.52011 6.12896 8.27677C6.37396 8.03344 6.76896 8.03344 7.01229 8.27844L8.99979 10.2743L10.9873 8.27844C11.2306 8.03344 11.6256 8.03344 11.8706 8.27677C12.1148 8.52011 12.1156 8.91594 11.8723 9.16094L9.44229 11.6001C9.32563 11.7184 9.16563 11.7843 8.99979 11.7843Z" fill="white"/>
                                <mask id="mask0_2489_3442" style="mask-type:alpha" maskUnits="userSpaceOnUse" x="0" y="4" width="18" height="12">
                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M0.666016 4.44385H17.3325V15.8972H0.666016V4.44385Z" fill="white"/>
                                </mask>
                                <g mask="url(#mask0_2489_3442)">
                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M13.6452 15.8972H4.36185C2.32435 15.8972 0.666016 14.2397 0.666016 12.2013V8.13051C0.666016 6.09718 2.32018 4.44385 4.35435 4.44385H5.13852C5.48352 4.44385 5.76352 4.72385 5.76352 5.06885C5.76352 5.41385 5.48352 5.69385 5.13852 5.69385H4.35435C3.00935 5.69385 1.91602 6.78635 1.91602 8.13051V12.2013C1.91602 13.5505 3.01268 14.6472 4.36185 14.6472H13.6452C14.9885 14.6472 16.0827 13.553 16.0827 12.2097V8.13968C16.0827 6.79051 14.9852 5.69385 13.6377 5.69385H12.861C12.516 5.69385 12.236 5.41385 12.236 5.06885C12.236 4.72385 12.516 4.44385 12.861 4.44385H13.6377C15.6752 4.44385 17.3327 6.10218 17.3327 8.13968V12.2097C17.3327 14.243 15.6777 15.8972 13.6452 15.8972Z" fill="white"/>
                                </g>
                            </svg>
                            </picture>
                        </a>
                    </div>
                </div>
                <div class="bot">
                    <picture>
                        <img src="./dist/image/Rectangle 243.png" alt="">
                    </picture>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="fee-chart">
    <div class="container">
        <div class="fee-chart-title">
            <h3>Discount Policies</h3>
        </div>
        <div class="fee-chart-content">
            <div class="title">
                <h5>Tuition fee</h5>
            </div>
            <div class="fee-chart-content-info">
                <div class="content-item">
                    <h5>Lorem ipsum</h5>
                    <div class="content-item-text">
                        <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Pellentesque habitant morbi tristique senectus. Arcu ac tortor dignissim convallis. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Pellentesque habitant morbi tristique senectus. Arcu ac tortor dignissim convallis. </span>
                    </div>
                </div>
                <div class="content-item">
                    <h5>Lorem ipsum</h5>
                    <div class="content-item-text">
                        <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Pellentesque habitant morbi tristique senectus. Arcu ac tortor dignissim convallis. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Pellentesque habitant morbi tristique senectus. Arcu ac tortor dignissim convallis. </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="fee-chart-content">
            <div class="title">
                <h5>Late enrolment</h5>
            </div>
            <div class="fee-chart-content-list">
                <div class="top">
                    <div class="top-text">
                        <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Pellentesque habitant morbi tristique senectus. Arcu ac tortor dignissim convallis. </span>
                    </div>
                    <div class="top-button">
                        <a class="top-button-detail" href="#">View details</a>
                        <a class="top-button-download" href="#">
                            <div class="text">
                                <span>Download fee chart</span>
                            </div>
                            <picture>
                            <svg xmlns="http://www.w3.org/2000/svg" width="18" height="16" viewBox="0 0 18 16" fill="none">
                                <path fill-rule="evenodd" clip-rule="evenodd" d="M9 11.7843C8.655 11.7843 8.375 11.5043 8.375 11.1593V1.12512C8.375 0.780122 8.655 0.500122 9 0.500122C9.345 0.500122 9.625 0.780122 9.625 1.12512V11.1593C9.625 11.5043 9.345 11.7843 9 11.7843Z" fill="white"/>
                                <path fill-rule="evenodd" clip-rule="evenodd" d="M8.99979 11.7843C8.83396 11.7843 8.67396 11.7184 8.55729 11.6001L6.12729 9.16094C5.88396 8.91594 5.88479 8.52011 6.12896 8.27677C6.37396 8.03344 6.76896 8.03344 7.01229 8.27844L8.99979 10.2743L10.9873 8.27844C11.2306 8.03344 11.6256 8.03344 11.8706 8.27677C12.1148 8.52011 12.1156 8.91594 11.8723 9.16094L9.44229 11.6001C9.32563 11.7184 9.16563 11.7843 8.99979 11.7843Z" fill="white"/>
                                <mask id="mask0_2489_3442" style="mask-type:alpha" maskUnits="userSpaceOnUse" x="0" y="4" width="18" height="12">
                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M0.666016 4.44385H17.3325V15.8972H0.666016V4.44385Z" fill="white"/>
                                </mask>
                                <g mask="url(#mask0_2489_3442)">
                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M13.6452 15.8972H4.36185C2.32435 15.8972 0.666016 14.2397 0.666016 12.2013V8.13051C0.666016 6.09718 2.32018 4.44385 4.35435 4.44385H5.13852C5.48352 4.44385 5.76352 4.72385 5.76352 5.06885C5.76352 5.41385 5.48352 5.69385 5.13852 5.69385H4.35435C3.00935 5.69385 1.91602 6.78635 1.91602 8.13051V12.2013C1.91602 13.5505 3.01268 14.6472 4.36185 14.6472H13.6452C14.9885 14.6472 16.0827 13.553 16.0827 12.2097V8.13968C16.0827 6.79051 14.9852 5.69385 13.6377 5.69385H12.861C12.516 5.69385 12.236 5.41385 12.236 5.06885C12.236 4.72385 12.516 4.44385 12.861 4.44385H13.6377C15.6752 4.44385 17.3327 6.10218 17.3327 8.13968V12.2097C17.3327 14.243 15.6777 15.8972 13.6452 15.8972Z" fill="white"/>
                                </g>
                            </svg>
                            </picture>
                        </a>
                    </div>
                </div>
                <div class="bot">
                    <picture>
                        <img src="./dist/image/Rectangle 243.png" alt="">
                    </picture>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="fee-chart">
    <div class="container">
        <div class="fee-chart-title">
            <h3>Tuition and fee schedule</h3>
        </div>
        <div class="fee-chart-content">
            <div class="title">
                <h5>Tuition fee</h5>
            </div>
            <div class="fee-chart-content-info">
                <div class="content-item">
                    <h5>Lorem ipsum</h5>
                    <div class="content-item-text">
                        <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Pellentesque habitant morbi tristique senectus. Arcu ac tortor dignissim convallis. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Pellentesque habitant morbi tristique senectus. Arcu ac tortor dignissim convallis. </span>
                    </div>
                </div>
                <div class="content-item">
                    <h5>Lorem ipsum</h5>
                    <div class="content-item-text">
                        <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Pellentesque habitant morbi tristique senectus. Arcu ac tortor dignissim convallis. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Pellentesque habitant morbi tristique senectus. Arcu ac tortor dignissim convallis. </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="fee-chart">
    <div class="container">
        <div class="fee-chart-title">
            <h3>Regulation on withdrawal and refund</h3>
        </div>
        <div class="fee-chart-content">
            <div class="title">
                <h5>Tuition fee</h5>
            </div>
            <div class="fee-chart-content-info">
                <div class="content-item">
                    <h5>Lorem ipsum</h5>
                    <div class="content-item-text">
                        <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Pellentesque habitant morbi tristique senectus. Arcu ac tortor dignissim convallis. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Pellentesque habitant morbi tristique senectus. Arcu ac tortor dignissim convallis. </span>
                    </div>
                </div>
                <div class="content-item">
                    <h5>Lorem ipsum</h5>
                    <div class="content-item-text">
                        <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Pellentesque habitant morbi tristique senectus. Arcu ac tortor dignissim convallis. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Pellentesque habitant morbi tristique senectus. Arcu ac tortor dignissim convallis. </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php include 'footer.php' ?>