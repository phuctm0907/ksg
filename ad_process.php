<?php include 'header.php' ?>
<section class="banner">
    <picture>
        <img src="../dist/image/Rectangle 197.png" alt="">
    </picture>
    <div class="text">
        <ul class="text-top">
            <li>
                <a href="#">Home</a>
                <span>></span>
            </li>
            <li>
                <a href="#">About Us</a>
                <span>></span>
            </li>
            <li>
                <a href="#">Mission & Vision</a>
                <span>></span>
            </li>
        </ul>
        <div class="text-bot">
            <h2>Principal’s Greeting</h2>
        </div>
    </div>
</section>
<section class="ad-regulation">
    <div class="container">
        <div class="row">
            <div class="col-3 ad-regulation-left">
                <div class="content">
                    <div class="title">
                        <h4>Admissions Regulations</h4>
                    </div>
                    <div class="align-items-start">
                        <ul class="list list-group">  <!--id="list-example" -->
                            <li><a class="active list-group-item list-group-item-action" href="#list-d1">Step 1: Application</a></li>    
                            <li><a class="list-group-item list-group-item-action" href="#list-d2">Step 2: Assesssment</a></li>    
                            <li><a class="list-group-item list-group-item-action" href="#list-d3">Step 3: Enrolment</a></li>    
                        </ul>
                    </div>
                </div>
            </div> 
            <!-- data-bs-spy="scroll" data-bs-target="#list-example" data-bs-offset="0" tabindex="0" -->
            <div class="scrollspy-example ad-regulation-right col-8" >
                <div class="ad-regulation-right-item ad-process-right-item" id="list-d1">
                    <h4 class="title">Step 1: Application</h4>
                    <picture>
                        <img src="../dist/image/Rectangle 234.png" alt="">
                    </picture>
                    <div class="content">
                        <span>A completed application form is required for each child, with an application fee payable. Parents will be contacted by our Admissions team within 5 working days of receiving the completed form and admissions fee.</span>
                        <span>Please note: The fee is non-refundable and non-transferable. The application form relates to the school year for which the pupil is applying, and submission of an application and payment of the application fee does not guarantee a place at the school.</span>
                    </div>
                </div>
                <div class="ad-regulation-right-item ad-process-right-item" id="list-d2">
                    <h4 class="title">Step 2: Assesssment</h4>
                    <picture>
                        <img src="../dist/image/Rectangle 234.png" alt="">
                    </picture>
                    <div class="content">
                        <span>An assessment will be scheduled as soon as possible. Parents will be notified via an initial email, followed by confirmation five days prior to the assessment.</span>
                        <span>The assessment is divided into two parts, both of which will take place on the same day:</span>
                        <div class="content-list">
                            <ul>
                                <li><span>Part 1: Digital assessments to determine the pupil’s English, maths and reasoning abilities</span></li>
                                <li><span>Part 2: Interviews with parents and the pupil</span></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="ad-regulation-right-item ad-process-right-item" id="list-d3">
                    <h4 class="title">Step 3: Enrolment</h4>
                    <picture>
                        <img src="../dist/image/Rectangle 234.png" alt="">
                    </picture>
                    <div class="content">
                        <span>The Admissions team will contact parents within 10 working days to inform them of the assessment results and the school’s decision. If results merit an offer of place, parents will receive a Letter of Acceptance for their child(ren) via email accompanied by a checklist of items which must be provided to confirm the applicant’s place at the school.</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script src="../dist/js/ad_regulation-process.js"></script>
<?php include 'footer.php' ?>