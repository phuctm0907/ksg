<?php include 'header.php' ?>
    <section class="banner">
        <picture>
            <img src="./dist/image/Rectangle 197.png" alt="">
        </picture>
        <div class="text">
            <ul class="text-top">
                <li>
                    <a href="#">Home</a>
                    <span>></span>
                </li>
                <li>
                    <a href="#">Academic</a>
                </li>
            </ul>
            <div class="text-bot">
                <h2>Academic</h2>
            </div>
        </div>
    </section>
    <section class="about-academic" style="background-image: url(./dist/image/Rectangles.png);">
        <div class="container">
            <div class="about">
                <div class="row flex-note">
                    <div class="col-md-6">
                        <div class="content">
                            <h3>KGS International School</h3>
                            <p>At the heart of KGS International School's vision and mission is the commitment to create
                                a transformative educational environment. We envision a school where students are not
                                only well-versed in academic excellence but are also equipped with a profound
                                understanding of the world, a global mindset, and a sense of responsibility. Our vision
                                extends beyond conventional educational paradigms; we aim to graduate individuals who,
                                with confidence and resilience, lead the way in navigating the complexities of our
                                ever-evolving global society.</p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="img">
                            <img src="./dist/image/2a37bad004b6ef87287ba3c53420c5a0.png" alt="">
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
   <section class="content-primary">
       <div class="container">
           <div class="about-content">
               <div class="row list-row list-1">
                   <div class="col-md-6">
                       <div class="img">
                           <img src="./dist/image/6e8ce40e3f648c87dbc223c4999cd683.png" alt="">
                       </div>
                   </div>
                   <div class="col-md-5">
                       <div class="note">
                           <h3>
                               Beyond the classroom
                           </h3>
                           <p>During Years 1 to 6 our children learn a great deal beyond the curriculum. Their lessons are enriched with trips, workshops and visits to inspire and encourage their engagement with the wider world. The combination of academic lessons, pastoral sessions, and a range of arts and sports lessons and activities ensure our children experience an extensive education.</p>
                       </div>
                   </div>
               </div>
               <div class="row list-row">

                   <div class="col-md-5">
                       <div class="note">
                           <h3>
                               Home Economics And
                               Lifetime Health (HEALTH)
                           </h3>
                           <p>During Years 1 to 6 our children learn a great deal beyond the curriculum. Their lessons are enriched with trips, workshops and visits to inspire and encourage their engagement with the wider world. The combination of academic lessons, pastoral sessions, and a range of arts and sports lessons and activities ensure our children experience an extensive education.</p>
                       </div>
                   </div>
                   <div class="col-md-7">
                       <div class="img">
                           <img src="./dist/image/6e8ce40e3f648c87dbc223c4999cd683.png" alt="">
                       </div>
                   </div>
               </div>
           </div>
       </div>
   </section>
    <section class="vision form-primary">
        <div class="container">
            <h3>If you would like to read more about our Primary School, please click below.</h3>
            <div class="d-flex">
                <div class="col-md-6">
                    <div class="dowloadpdf">
                        <a href="/images/myw3schoolsimage.jpg" class="text-dow" download><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                <path d="M16 22.75H8C4.35 22.75 2.25 20.65 2.25 17V7C2.25 3.35 4.35 1.25 8 1.25H16C19.65 1.25 21.75 3.35 21.75 7V17C21.75 20.65 19.65 22.75 16 22.75ZM8 2.75C5.14 2.75 3.75 4.14 3.75 7V17C3.75 19.86 5.14 21.25 8 21.25H16C18.86 21.25 20.25 19.86 20.25 17V7C20.25 4.14 18.86 2.75 16 2.75H8Z" fill="#292D32"/>
                                <path d="M18.5 9.25H16.5C14.98 9.25 13.75 8.02 13.75 6.5V4.5C13.75 4.09 14.09 3.75 14.5 3.75C14.91 3.75 15.25 4.09 15.25 4.5V6.5C15.25 7.19 15.81 7.75 16.5 7.75H18.5C18.91 7.75 19.25 8.09 19.25 8.5C19.25 8.91 18.91 9.25 18.5 9.25Z" fill="#292D32"/>
                                <path d="M12 13.75H8C7.59 13.75 7.25 13.41 7.25 13C7.25 12.59 7.59 12.25 8 12.25H12C12.41 12.25 12.75 12.59 12.75 13C12.75 13.41 12.41 13.75 12 13.75Z" fill="#292D32"/>
                                <path d="M16 17.75H8C7.59 17.75 7.25 17.41 7.25 17C7.25 16.59 7.59 16.25 8 16.25H16C16.41 16.25 16.75 16.59 16.75 17C16.75 17.41 16.41 17.75 16 17.75Z" fill="#292D32"/>
                            </svg>
                            Primary School.pdf
                        </a>
                        <div class="btn-more">
                            <a href="" >Download
                                <svg xmlns="http://www.w3.org/2000/svg" width="18" height="16" viewBox="0 0 18 16" fill="none">
                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M9 11.7842C8.655 11.7842 8.375 11.5042 8.375 11.1592V1.125C8.375 0.78 8.655 0.5 9 0.5C9.345 0.5 9.625 0.78 9.625 1.125V11.1592C9.625 11.5042 9.345 11.7842 9 11.7842Z" fill="white"/>
                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M8.99979 11.7843C8.83396 11.7843 8.67396 11.7184 8.55729 11.6001L6.12729 9.16094C5.88396 8.91594 5.88479 8.52011 6.12896 8.27677C6.37396 8.03344 6.76896 8.03344 7.01229 8.27844L8.99979 10.2743L10.9873 8.27844C11.2306 8.03344 11.6256 8.03344 11.8706 8.27677C12.1148 8.52011 12.1156 8.91594 11.8723 9.16094L9.44229 11.6001C9.32563 11.7184 9.16563 11.7843 8.99979 11.7843Z" fill="white"/>
                                    <mask id="mask0_2746_1225" style="mask-type:alpha" maskUnits="userSpaceOnUse" x="0" y="4" width="18" height="12">
                                        <path fill-rule="evenodd" clip-rule="evenodd" d="M0.666016 4.44385H17.3325V15.8972H0.666016V4.44385Z" fill="white"/>
                                    </mask>
                                    <g mask="url(#mask0_2746_1225)">
                                        <path fill-rule="evenodd" clip-rule="evenodd" d="M13.6452 15.8972H4.36185C2.32435 15.8972 0.666016 14.2397 0.666016 12.2013V8.13051C0.666016 6.09718 2.32018 4.44385 4.35435 4.44385H5.13852C5.48352 4.44385 5.76352 4.72385 5.76352 5.06885C5.76352 5.41385 5.48352 5.69385 5.13852 5.69385H4.35435C3.00935 5.69385 1.91602 6.78635 1.91602 8.13051V12.2013C1.91602 13.5505 3.01268 14.6472 4.36185 14.6472H13.6452C14.9885 14.6472 16.0827 13.553 16.0827 12.2097V8.13968C16.0827 6.79051 14.9852 5.69385 13.6377 5.69385H12.861C12.516 5.69385 12.236 5.41385 12.236 5.06885C12.236 4.72385 12.516 4.44385 12.861 4.44385H13.6377C15.6752 4.44385 17.3327 6.10218 17.3327 8.13968V12.2097C17.3327 14.243 15.6777 15.8972 13.6452 15.8972Z" fill="white"/>
                                    </g>
                                </svg>
                            </a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <picture>
            <img src="./dist/image/Group 1000006138.png" alt="">
        </picture>
    </section>
    <script src="./dist/js/ad_regulation-process.js"></script>
<?php include 'footer.php' ?>