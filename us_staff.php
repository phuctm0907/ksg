<?php include 'header.php' ?>
<section class="banner">
    <picture>
        <img src="./dist/image/Rectangle 197.png" alt="">
    </picture>
    <div class="text">
        <ul class="text-top">
            <li>
                <a href="#">Home</a>
                <span>></span>
            </li>
            <li>
                <a href="#">About Us</a>
                <span>></span>
            </li>
            <li>
                <a href="#">Mission & Vision</a>
                <span>></span>
            </li>
        </ul>
        <div class="text-bot">
            <h2>Principal’s Greeting</h2>
        </div>
    </div>
</section>
<section class="staff">
    <div class="container">
        <ul class="staff-list">
            <li class="staff-list-item staff-show-d1">
                <picture>
                    <img src="./dist/image/Rectangle 210.png" alt="">
                </picture>
                <div class="title">
                    <h4>Jeong Young Oh</h4>
                    <div class="text">
                        <span>Principal</span>
                    </div>
                </div>
            </li>
            <script !src="">
                $(document).ready(function(){
                    $('.staff-show-d1').click(function(){
                        $('.pickup-show-d1').addClass('show');
                    })
                });
            </script>
            <li class="staff-list-item staff-show-d1">
                <picture>
                    <img src="./dist/image/Rectangle 210.png" alt="">
                </picture>
                <div class="title">
                    <h4>Jeong Young Oh</h4>
                    <div class="text">
                        <span>Principal</span>
                    </div>
                </div>
            </li>
            <li class="staff-list-item staff-show-d1">
                <picture>
                    <img src="./dist/image/Rectangle 210.png" alt="">
                </picture>
                <div class="title">
                    <h4>Jeong Young Oh</h4>
                    <div class="text">
                        <span>Principal</span>
                    </div>
                </div>
            </li>
            <li class="staff-list-item staff-show-d1">
                <picture>
                    <img src="./dist/image/Rectangle 210.png" alt="">
                </picture>
                <div class="title">
                    <h4>Jeong Young Oh</h4>
                    <div class="text">
                        <span>Principal</span>
                    </div>
                </div>
            </li>
            <li class="staff-list-item staff-show-d1">
                <picture>
                    <img src="./dist/image/Rectangle 210.png" alt="">
                </picture>
                <div class="title">
                    <h4>Jeong Young Oh</h4>
                    <div class="text">
                        <span>Principal</span>
                    </div>
                </div>
            </li>
            <li class="staff-list-item staff-show-d1">
                <picture>
                    <img src="./dist/image/Rectangle 210.png" alt="">
                </picture>
                <div class="title">
                    <h4>Jeong Young Oh</h4>
                    <div class="text">
                        <span>Principal</span>
                    </div>
                </div>
            </li>
            <li class="staff-list-item staff-show-d1">
                <picture>
                    <img src="./dist/image/Rectangle 210.png" alt="">
                </picture>
                <div class="title">
                    <h4>Jeong Young Oh</h4>
                    <div class="text">
                        <span>Principal</span>
                    </div>
                </div>
            </li>
            <li class="staff-list-item staff-show-d1">
                <picture>
                    <img src="./dist/image/Rectangle 210.png" alt="">
                </picture>
                <div class="title">
                    <h4>Jeong Young Oh</h4>
                    <div class="text">
                        <span>Principal</span>
                    </div>
                </div>
            </li>
            <li class="staff-list-item staff-show-d1">
                <picture>
                    <img src="./dist/image/Rectangle 210.png" alt="">
                </picture>
                <div class="title">
                    <h4>Jeong Young Oh</h4>
                    <div class="text">
                        <span>Principal</span>
                    </div>
                </div>
            </li>
            <li class="staff-list-item staff-show-d1">
                <picture>
                    <img src="./dist/image/Rectangle 210.png" alt="">
                </picture>
                <div class="title">
                    <h4>Jeong Young Oh</h4>
                    <div class="text">
                        <span>Principal</span>
                    </div>
                </div>
            </li>
            <li class="staff-list-item staff-show-d1">
                <picture>
                    <img src="./dist/image/Rectangle 210.png" alt="">
                </picture>
                <div class="title">
                    <h4>Jeong Young Oh</h4>
                    <div class="text">
                        <span>Principal</span>
                    </div>
                </div>
            </li>
            <li class="staff-list-item staff-show-d1">
                <picture>
                    <img src="./dist/image/Rectangle 210.png" alt="">
                </picture>
                <div class="title">
                    <h4>Jeong Young Oh</h4>
                    <div class="text">
                        <span>Principal</span>
                    </div>
                </div>
            </li>
            <li class="staff-list-item staff-show-d1">
                <picture>
                    <img src="./dist/image/Rectangle 210.png" alt="">
                </picture>
                <div class="title">
                    <h4>Jeong Young Oh</h4>
                    <div class="text">
                        <span>Principal</span>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</section>
<section class="pickup">
    <ul class="pickup-info">
        <li class="pickup-info-child pickup-show-d1">
            <div class="pickup-info-child-content">
                <div class="icon pickup-show-close-d1">
                    <i class='bx bx-x' style='color:#ffffff'  ></i>
                </div>
                <div class="title">
                    <h4>Staff</h4>
                </div>
                <div class="detail">
                    <picture>
                        <img src="./dist/image/Rectangle 2100.png" alt="">
                    </picture>
                    <div class="detail-info">
                        <div class="detail-info-title">
                            <h4>Jeong Young Oh</h4>
                            <div class="text">
                                <span>Principal</span>
                            </div>
                        </div>
                        <ul class="detail-info-describe">
                            <li><span>Bachelor's degree from Edinburgh University and Master's degree from Oxford Brookes University</span></li>
                            <li><span>Bachelor's degree from Edinburgh University and Master's degree from Oxford Brookes University</span></li>
                            <li><span>Bachelor's degree from Edinburgh University and Master's degree from Oxford Brookes University</span></li>
                            <li><span>Bachelor's degree from Edinburgh University and Master's degree from Oxford Brookes University</span></li>
                            <li><span>Bachelor's degree from Edinburgh University and Master's degree from Oxford Brookes University</span></li>
                            <li><span>Bachelor's degree from Edinburgh University and Master's degree from Oxford Brookes University</span></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="pickup-info-child-gray pickup-show-gray-d1"></div>
        </li>
        <script !src="">
            $(document).ready(function(){
                $('.pickup-show-close-d1').click(function(){
                    $('.pickup-show-d1').removeClass('show');
                })
                $('.pickup-show-gray-d1').click(function(){
                    $('.pickup-show-d1').removeClass('show');
                })
            });
        </script>
    </ul>
</section>
<?php include 'footer.php' ?>