<?php include 'header.php' ?>
<section class="banner">
    <picture>
        <img src="./dist/image/Rectangle 197.png" alt="">
    </picture>
    <div class="text">
        <ul class="text-top">
            <li>
                <a href="#">Home</a>
                <span>></span>
            </li>
            <li>
                <a href="#">Parents</a>
                <span>></span>
            </li>
            <li>
                <a href="#">Photo Gallery</a>
                <span>></span>
            </li>
        </ul>
        <div class="text-bot">
            <h2>Photo Gallery</h2>
        </div>
    </div>
</section>
<section class="gallery">
    <div class="container">
        <div class="time">
            <div class="time-left">
                <div class="time-left-date time-left-date-d1">
                    <select id="month_d1">
                        <option value="hide">Month</option>
                        <option value="january">January</option>
                        <option value="february">February</option>
                        <option value="march">March</option>
                        <option value="april">April</option>
                        <option value="may">May</option>
                        <option value="june">June</option>
                        <option value="july">July</option>
                        <option value="august">August</option>
                        <option value="september">September</option>
                        <option value="october">October</option>
                        <option value="november">November</option>
                        <option value="december">December</option>
                    </select>
                    <select id="year_d1">
                        <option value="hide">Year</option>
                        <option value="2020">2020</option>
                        <option value="2021">2021</option>
                        <option value="2022">2022</option>
                        <option value="2023">2023</option>
                        <option value="2024">2024</option>
                        <option value="2025">2025</option>
                    </select>
                </div>
                <div class="time-left-button">
                    <form action="#">
                        <button type="submit" name="s">Search</button>
                    </form>
                </div>
            </div>
        </div>

        <div class="grid-wrapper" id="js-gallery">
            <div class="library-image" data-src="">
                <img src="http://serena.wecan-group.info/wp-content/uploads/2022/11/HVG_1262-scaled.jpg" alt="image 1">
            </div>
            <div class="library-image" data-src="">
                <img src="http://serena.wecan-group.info/wp-content/uploads/2022/11/HVG_1262-scaled.jpg" alt="image 1">
            </div>
            <div class="library-image" data-src="">
                <img src="http://serena.wecan-group.info/wp-content/uploads/2022/11/HVG_1262-scaled.jpg" alt="image 1">
            </div>
            <div class="library-image" data-src="">
                <img src="http://serena.wecan-group.info/wp-content/uploads/2022/11/HVG_1262-scaled.jpg" alt="image 1">
            </div>
            <div class="width-2 library-image" data-src="">
                <img src="http://serena.wecan-group.info/wp-content/uploads/2022/11/HVG_1262-scaled.jpg" alt="image 1">
            </div>
            <div class="library-image" data-src="">
                <img src="http://serena.wecan-group.info/wp-content/uploads/2022/11/HVG_1262-scaled.jpg" alt="image 1">
            </div>
            <div class="height-2 library-image" data-src="">
                <img src="http://serena.wecan-group.info/wp-content/uploads/2022/11/HVG_1262-scaled.jpg" alt="image 1">
            </div>
            <div class="library-image" data-src="">
                <img src="http://serena.wecan-group.info/wp-content/uploads/2022/11/HVG_1262-scaled.jpg" alt="image 1">
            </div>
            <div class="width-2 library-image" data-src="">
                <img src="http://serena.wecan-group.info/wp-content/uploads/2022/11/HVG_1262-scaled.jpg" alt="image 1">
            </div>
        </div>
        <div class="btn-load">
            <button>Load more</button>
        </div>
    </div>
</section>
<script src="./dist/js/ad_regulation-process.js"></script>
<?php include 'footer.php' ?>
<script>
    $(document).ready(function() {
        const $lgContainer = document.getElementById("js-gallery");
        const lg = lightGallery($lgContainer, {
            animateThumb: true,
            allowMediaOverlap: true,
            toggleThumb: true,
            download: false,
            speed: 500,
            slideShowAutoplay: true,
            plugins: [lgThumbnail, lgFullscreen],
            fullScreen: true,
            showZoomInOutIcons: false,
            actualSize: true,
        });
    });
</script>