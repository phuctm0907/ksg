<?php include 'header.php' ?>
    <section class="banner">
        <picture>
            <img src="./dist/image/Rectangle 197.png" alt="">
        </picture>
        <div class="text">
            <ul class="text-top">
                <li>
                    <a href="#">Home</a>
                    <span>></span>
                </li>
                <li>
                    <a href="#">Recruitment</a>
                </li>

            </ul>
            <div class="text-bot">
                <h2>Recruitment</h2>
            </div>
        </div>
    </section>
    <section class="section-recruitment">
        <div class="container">
            <div class="input-search">
                <div class="col-md-6 col-12">
                    <div class="search">
                        <select class="form-select" aria-label="Default select example">
                            <option selected>Departments</option>
                            <option value="1">One</option>
                            <option value="2">Two</option>
                            <option value="3">Three</option>
                        </select>
                        <input type="text" class="form-control" placeholder="Enter job keyword">
                        <button type="button" class="btn btn-secondary">Secondary</button>
                    </div>
                </div>
            </div>
            <div class="list-recruitment">
                <div class="item-re">
                    <hr>
                    <div class="item-recruitment">

                        <div class="item-left">
                            <span class="status">New</span>
                            <h4 class="title">TEMPORARY MATHS CO-TEACHER</h4>
                        </div>
                        <div class="item-right">
                            <span class="date">2023.11.16</span>
                            <a href="">Apply now</a>
                        </div>

                    </div>
                    <hr>
                </div>
                <div class="item-re">
                    <hr>
                    <div class="item-recruitment">

                        <div class="item-left">
                            <span class="status">New</span>
                            <h4 class="title">TEMPORARY MATHS CO-TEACHER</h4>
                        </div>
                        <div class="item-right">
                            <span class="date">2023.11.16</span>
                            <a href="">Apply now</a>
                        </div>

                    </div>
                    <hr>
                </div>
                <div class="item-re">
                    <hr>
                    <div class="item-recruitment">

                        <div class="item-left">
                            <span class="status">New</span>
                            <h4 class="title">TEMPORARY MATHS CO-TEACHER</h4>
                        </div>
                        <div class="item-right">
                            <span class="date">2023.11.16</span>
                            <a href="">Apply now</a>
                        </div>

                    </div>
                    <hr>
                </div>
                <div class="item-re">
                    <hr>
                    <div class="item-recruitment">

                        <div class="item-left">
                            <span class="status">New</span>
                            <h4 class="title">TEMPORARY MATHS CO-TEACHER</h4>
                        </div>
                        <div class="item-right">
                            <span class="date">2023.11.16</span>
                            <a href="">Apply now</a>
                        </div>

                    </div>
                    <hr>
                </div>
                <div class="item-re">
                    <hr>
                    <div class="item-recruitment">

                        <div class="item-left">
                            <span class="status">New</span>
                            <h4 class="title">TEMPORARY MATHS CO-TEACHER</h4>
                        </div>
                        <div class="item-right">
                            <span class="date">2023.11.16</span>
                            <a href="">Apply now</a>
                        </div>

                    </div>
                    <hr>
                </div>
            </div>
        </div>
    </section>


<?php include 'footer.php' ?>